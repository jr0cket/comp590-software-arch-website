<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Notes from Jan. 27 lecture</title>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <link rel="stylesheet" type="text/css" href="/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>

  <body>
    <header class="constrained-width">
      <a href="/"><img src="/logo.png" width="260" height="95" alt="App Lab logo"></a>
      <h1>COMP 590: Software Architecture</h1>
    </header>

    <nav id="main-nav">
      <ul>
        <li><a href="/">home</a></li>
        <li><a href="/syllabus/">syllabus</a></li>
        <li><a href="/calendar/">calendar</a></li>
        <li><a href="/assignments/">assignments</a></li>
        <li><a href="/blog/">announcements</a></li>
      </ul>
    </nav>

    <main class="constrained-width">
      <article>
<h1>Notes from Jan. 27 lecture</h1><h3 id="announcements">Announcements</h3>
<ul>
<li>Music: <em>Polyphonic Song: Edi beo thu hevene quene</em> by Anonymous 4, on the album An English Ladymass: Medieval Chant and Polyphony.
<ul>
<li>This is an example of medieval chant.</li>
<li>We'll be talking about abstraction today, and I figure this is fairly abstract music.</li>
</ul></li>
<li>Feedback for me? Use the form posted to the course home page.</li>
<li>Need to request an absence for a midterm exam? Use the form posted to the course home page. Remember: at most 1 absence allowed per person.</li>
<li>Some people have successfully completed Assignment 1 with full credit, so we now know it's possible. (If you didn't get an email from Gradescope, let me know.) We did identify a lack of specificity about what to submit exactly, which I clarified in the assignment submission instructions.</li>
<li>The <a href="https://clojure.org/api/cheatsheet">Clojure Cheatsheet</a> is a nice thing to have up while coding.</li>
</ul>
<h3 id="warm-up-exercise">Warm-up exercise</h3>
<blockquote>
<p>Define a Clojure function named <code>double-items</code>, that takes a sequence of numbers and returns a sequence of the same numbers doubled.</p>
</blockquote>
<p>Solution:</p>
<div class="sourceCode"><pre class="sourceCode clojure"><code class="sourceCode clojure">(<span class="bu">defn</span><span class="fu"> double-items </span>[xs]
  (<span class="kw">map</span> #(<span class="kw">*</span> <span class="dv">2</span> <span class="va">%</span>) xs))</code></pre></div>
<p>You'll definitely need to know how to use <code>defn</code> in your assignments.</p>
<h3 id="clem-the-clojure-error-mediator">Clem, the Clojure Error Mediator</h3>
<p>Here are <a href="https://applab.unc.edu/posts/2020/01/17/integrating-clem-cursive/">instructions for setting up Clem in Cursive</a>.</p>
<p>In a Clem-enabled REPL, if an exception occurs, your exception will be sent to Clem's online database of Clojure exceptions and explanations. If an explanation exists for your exception, it will be printed in your REPL. If not, you will have the opportunity to write an explanation yourself.</p>
<h3 id="cursive-tips">Cursive tips</h3>
<p>If you are annoyed that some edits you want to make to Clojure code are prevented, you can disable that feature under edit -&gt; structural editing -&gt; toggle structural editing. (You can also do this globally at settings -&gt; editor -&gt; general -&gt; smart keys -&gt; use structural editing.) Cursive is attempting to keep your parens balanced. This is good if you know structural editing shortcuts, but if not, it can get in your way. You can <a href="https://cursive-ide.com/userguide/paredit.html">read more about structural editing in Cursive here</a>.</p>
<p>It may also be helpful to let Cursive indent your code for you. There's <a href="https://guide.clojure.style/">a pretty consistent style</a> that people use for Clojure code. It's probably a little different than languages you're used to, but it can be quite readable once you get used to it. In Cursive, use code -&gt; reformat code (and similar options) to do this. There's more info <a href="https://cursive-ide.com/userguide/formatting.html">here</a>.</p>
<h3 id="clojure-debugging">Clojure debugging</h3>
<p>The best tool you have in debugging your code is not a debugger, it's your mind. Debuggers can certainly be useful, but they're no substitute for actually understanding what's happening.</p>
<p>This is a rich topic that I want to get into more later, but for now, let's just cover the basics.</p>
<p>To debug in Clojure, <em>isolate</em> your bug by iterating smaller in two dimensions:</p>
<ul>
<li>your code</li>
<li>your data</li>
</ul>
<p>In other words, find the smallest or simplest data structure that triggers the error, and likewise find the smallest bit of code that triggers the error. <a href="https://github.com/stuarthalloway">A prominent Clojure programmer</a> has named this exercise &quot;<a href="https://github.com/stuarthalloway/presentations/wiki/Running-with-Scissors">running with scissors</a>&quot;.</p>
<p>In order to iterate, you need (a) a reproducible error case and (b) a means of triggering it. The REPL is a great tool for (b), especially when combined with an editor. Put code that triggers the error in a <code>(comment ,,,)</code> block, along with any needed data definitions or require statements. Then start paring it down along the two dimensions.</p>
<p>When you have a reproducible and somewhat isolated bug, so-called &quot;printf debugging&quot; is great! It gives you visibility about what's actually happening. Because you've isolated it, you're not getting a bunch of distracting noise.</p>
<p>For example, consider this code to compute a sample standard deviation:</p>
<div class="sourceCode"><pre class="sourceCode clojure"><code class="sourceCode clojure">(<span class="bu">defn</span><span class="fu"> sum </span>[xs]
  (<span class="kw">reduce</span> <span class="kw">+</span> <span class="dv">0</span> xs))

(<span class="bu">defn</span><span class="fu"> mean </span>[xs]
  (<span class="kw">/</span> (sum xs)
     (<span class="kw">count</span> xs)))

(<span class="bu">defn</span><span class="fu"> square </span>[x]
  (<span class="kw">*</span> x x))

(<span class="bu">defn</span><span class="fu"> stdev </span>[xs]
  (<span class="kw">let</span> [this-mean (mean xs)
        errors (<span class="kw">map</span> #(<span class="kw">-</span> <span class="va">%</span> this-mean <span class="fl">1.0</span>) xs)
        squared-errors (<span class="kw">map</span> square errors)
        sq-err-sum (sum squared-errors)]
    (Math/sqrt (<span class="kw">/</span> sq-err-sum (<span class="kw">dec</span> (<span class="kw">count</span> xs))))))</code></pre></div>
<p>We're told that the correct answer for the following input:</p>
<pre><code>(stdev [10 12 23 23 16 23 21 16])</code></pre>
<p>...is <code>5.2372293656638</code> but instead we're getting an answer of <code>5.3452248382484875</code>. What's going on?</p>
<p>Let's get a REPL going with the test case. Then pare down the data to <code>[2 4 6]</code>, which should return <code>2</code> but instead returns <code>2.345207879911715</code>. Does <code>sum</code> give the right answer? Yes. Does <code>mean</code>? Yes. Does <code>square</code>? Yes. So the bug is in <code>stdev</code> somewhere.</p>
<p>Let's break down the operation of <code>stdev</code> into steps to isolate the bug further. The first <code>let</code> binding just calls <code>mean</code>, which we checked already. What does the second binding do? Computes something called errors. Does the result of <code>(-3.0 -1.0 1.0)</code> look right? Hmm, maybe not. It's oddly asymmetrical. What's going on? Ah, there's a <code>1.0</code> being subtracted from each error. Sure enough, that's not in the formula for stdev. Let's take it out. Now <code>stdev</code> gives the right answer, for both the reduced input and the full input.</p>
<p>Another tip for debugging: <code>prn</code> prints things in a Clojure-readable form, which is nice because e.g. you can distinguish between a string and a symbol. I like to tag my <code>prn</code> statements with a symbol describing what I'm printing. For example, if I'm checking what the value of the <code>args</code> var is, I'd say <code>(prn 'args args)</code>.</p>
<p>In the context of the autograder, all output gets captured, which would include your printing statements. To get around this, you can print to the standard error stream instead of the standard output stream, like so:</p>
<div class="sourceCode"><pre class="sourceCode clojure"><code class="sourceCode clojure">(<span class="kw">binding</span> [<span class="va">*out*</span> <span class="va">*err*</span>]
  (<span class="kw">prn</span> <span class="at">&#39;args</span> args))</code></pre></div>
<h3 id="abstraction-basics">Abstraction basics</h3>
<p>What is the essence of good software design? What is the key activity in making a program <em>good</em>?</p>
<blockquote>
<p>The essence of good software design is eliminating incidental complexity.</p>
</blockquote>
<p>Complexity is what makes programming hard. The key observation here, from Dr. Brooks in <a href="http://worrydream.com/refs/Brooks-NoSilverBullet.pdf">No Silver Bullet</a>, is that not all complexity is essential to the problem at hand. Much of it is smuggled in by:</p>
<ul>
<li>our approach</li>
<li>our language</li>
<li>our tools</li>
<li>our laziness</li>
</ul>
<p>As a simple example, consider the standard <code>for</code> loop:</p>
<pre><code>for (int i = 0; i &lt; arr.length; i++) {
  arr[i] *= 2;
}</code></pre>
<p>Lots of incidental complexity! We have to say:</p>
<ul>
<li>What type of variable to use</li>
<li>What variable name to use</li>
<li>Where to start counting</li>
<li>When to stop counting</li>
<li>What to do after each iteration</li>
<li>What to do at each iteration</li>
</ul>
<p>Only the last thing is essential! Consider the equivalent (though non-mutative) Clojure code:</p>
<pre><code>(map #(* 2 %) arr)</code></pre>
<p>Lesson: incidental complexity is difficult to spot because <em>we don't think about it</em>. We actively try <em>not</em> to think about it.</p>
<p>Languages differ not in what's possible to say, but in what you have to say to express a thought. Example: in English, if I'm talking about some number of paperclips, I have to say something about the quantity. Maybe &quot;a paperclip&quot;, &quot;some paperclips&quot;, &quot;a few&quot;, &quot;a couple&quot;, &quot;an unknown number&quot;, etc. At least I have to say &quot;paperclip&quot; or &quot;paperclips&quot;, distinguishing between one and multiple. I can't concisely express the idea of paperclips without specifying quantity. If quantity isn't relevant, then English is forcing me to have some incidental complexity.</p>
<p>In Spanish, I have to spend some words and syllables on the gender of nouns and making sure all the genders of words agree in all the right places. I don't have to do that in English, because e.g. &quot;paperclip&quot; carries no information about gender.</p>
<p>Key idea of incidental complexity: it depends what you want to do. Complexity that is incidental for one purpose might be essential for another. It depends.</p>
<p>Getting back to abstraction. Abstract, as a verb, comes from the Latin roots &quot;ab&quot;, meaning &quot;from&quot; or &quot;away&quot;, and &quot;trahere&quot;, meaning &quot;to pull&quot; or &quot;to draw&quot;. In other words, &quot;to draw away&quot;. (<a href="https://www.merriam-webster.com/dictionary/abstract">Source</a>.) A good word in English that conveys this idea more clearly is &quot;extract&quot;.</p>
<p>So in our for vs. map example above, map draws away various considerations that we don't need to care about. It extracts them from what you have to say. Those details aren't visible in the code.</p>
<p>This is good, <em>if and only if</em> the drawn away details are incidental rather than essential. Again, it depends. If they were essential, now we have to look in two places to get all the essence in our minds.</p>
<p>So the goal of good software design is to remove the incidental complexity, so that your code looks more like this:</p>
<div class="figure">
<img src="https://blog.ploeh.dk/content/binary/essential-accidental-complexity-shells-brooks-scenario.png" />

</div>
<p>or even this:</p>
<div class="figure">
<img src="https://blog.ploeh.dk/content/binary/essential-almost-no-accidental-complexity-shells.png" />

</div>
<p>rather than this:</p>
<div class="figure">
<img src="https://blog.ploeh.dk/content/binary/accidental-complexity-with-tiny-core-of-essential-complexity.png" />

</div>
<p>(Images from <a href="https://blog.ploeh.dk/2019/07/01/yes-silver-bullet/">here</a>.)</p>
<h3 id="more-on-clojure-16.-threading-macros">More on Clojure: 16. threading macros</h3>
<div class="sourceCode"><pre class="sourceCode clojure"><code class="sourceCode clojure">(<span class="kw">-&gt;</span> <span class="dv">7</span> <span class="kw">inc</span> <span class="kw">inc</span> <span class="kw">inc</span>)        <span class="co">; =&gt; 10 (the &quot;thread first&quot; macro)</span>
(<span class="kw">-&gt;</span> <span class="dv">7</span> <span class="kw">inc</span> (<span class="kw">/</span> <span class="dv">2</span>) (<span class="kw">+</span> <span class="dv">4</span>))    <span class="co">; =&gt; 8 (each result is 1st arg of next form)</span>
(<span class="kw">-&gt;&gt;</span> <span class="dv">3</span> <span class="kw">dec</span> (<span class="kw">-</span> <span class="dv">8</span>))         <span class="co">; =&gt; 6 (the &quot;thread last&quot; macro)</span>
<span class="co">;; sum of the first 100 non-neg ints divisible by 3</span>
(<span class="kw">-&gt;&gt;</span> (<span class="kw">range</span>)           <span class="co">; (notice laziness)</span>
     (<span class="kw">filter</span> #(<span class="kw">zero?</span> (<span class="kw">rem</span> <span class="va">%</span> <span class="dv">3</span>)))
     (<span class="kw">take</span> <span class="dv">100</span>)
     (<span class="kw">reduce</span> <span class="kw">+</span> <span class="dv">0</span>))
<span class="co">;; exercise: sum of the first 10 odd squares</span>
(<span class="kw">-&gt;&gt;</span> (<span class="kw">range</span>)
     (<span class="kw">map</span> #(<span class="kw">*</span> <span class="va">%</span> <span class="va">%</span>))
     (<span class="kw">filter</span> <span class="kw">odd?</span>)
     (<span class="kw">take</span> <span class="dv">10</span>)
     (<span class="kw">reduce</span> <span class="kw">+</span> <span class="dv">0</span>))
(<span class="kw">as-&gt;</span> {<span class="at">:a</span> <span class="dv">1</span>, <span class="at">:b</span> <span class="dv">2</span>} m      <span class="co">; as-&gt; lets you control...</span>
  (<span class="kw">update</span> m <span class="at">:a</span> <span class="kw">+</span> <span class="dv">10</span>)      <span class="co">; ...where the result goes...</span>
  (<span class="kw">if</span> update-b?           <span class="co">; ...in the next form.</span>
    (<span class="kw">update</span> m <span class="at">:b</span> <span class="kw">+</span> <span class="dv">10</span>)
    m))</code></pre></div>
      </article>
    </main>

    <footer class="constrained-width">
      <hr>
      <p>Static site generated from <a href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet">Markdown</a> content by <a href="https://pandoc.org/">pandoc</a> and <a href="https://gitlab.com/unc-app-lab/clj-ssg/">custom code</a> written in <a href="https://clojure.org/">Clojure</a>. The <a href="https://gitlab.com/jeff.terrell/comp590-software-arch-website/">site repository</a> is available.</p>
      <p>Site hosted by <a href="https://www.netlify.com/">Netlify</a>.</p>
    </footer>
  </body>
</html>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Syllabus</title>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <link rel="stylesheet" type="text/css" href="/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>

  <body>
    <header class="constrained-width">
      <a href="/"><img src="/logo.png" width="260" height="95" alt="App Lab logo"></a>
      <h1>COMP 590: Software Architecture</h1>
    </header>

    <nav id="main-nav">
      <ul>
        <li><a href="/">home</a></li>
        <li><a href="/syllabus/">syllabus</a></li>
        <li><a href="/calendar/">calendar</a></li>
        <li><a href="/assignments/">assignments</a></li>
        <li><a href="/blog/">announcements</a></li>
      </ul>
    </nav>

    <main class="constrained-width">
      <article>
<h1>Syllabus</h1><h3 id="contents">Contents</h3>
<ul>
<li><a href="#synopsis">Synopsis</a></li>
<li><a href="#general-course-info">General course info</a></li>
<li><a href="#instructor">Instructor</a></li>
<li><a href="#context-and-target-audience">Context and target audience</a></li>
<li><a href="#course-description">Course description</a></li>
<li><a href="#learning-objectives">Learning objectives</a></li>
<li><a href="#assignments">Assignments</a></li>
<li><a href="#grading-criteria">Grading criteria</a></li>
<li><a href="#attendance-and-class-participation">Attendance and class participation</a></li>
<li><a href="#lecture-topics">Lecture topics</a></li>
<li><a href="#deadlines-and-key-dates">Deadlines and key dates</a></li>
<li><a href="#classroom-policies">Classroom policies</a></li>
<li><a href="#honor-code">Honor code</a></li>
<li><a href="#dealing-with-stress">Dealing with stress</a></li>
<li><a href="#disclaimer">Disclaimer</a></li>
</ul>
<h3 id="synopsis">Synopsis</h3>
<p>Techniques to structure larger bodies of code: modules, testing, effects, refactoring, indirection, polymorphism, asynchrony, common architectures. Uses the <a href="https://clojure.org">Clojure</a> programming language.</p>
<h3 id="general-course-info">General course info</h3>
<ul>
<li>Term: spring 2019</li>
<li>Department: Computer Science (COMP)</li>
<li>Course number: 590: Software Architecture</li>
<li>Section number: 145</li>
<li>Time: MW 11:15am – 12:30pm</li>
<li>Location: Sitterson 014</li>
<li>Website: <a href="https://comp590-s20.cs.unc.edu" class="uri">https://comp590-s20.cs.unc.edu</a></li>
<li>Office hours: see <a href="../office-hours/">this page</a></li>
<li>Textbook: none</li>
<li>Prerequisite: COMP 410 (data structures)</li>
</ul>
<h3 id="instructor">Instructor</h3>
<ul>
<li>Name: Dr. Jeff Terrell</li>
<li>Email: <a href="mailto:terrell@cs.unc.edu">terrell@cs.unc.edu</a></li>
<li>Office: Sitterson 205</li>
<li>Web: <a href="http://terrell.web.unc.edu/" class="uri">http://terrell.web.unc.edu/</a></li>
</ul>
<h3 id="context-and-target-audience">Context and target audience</h3>
<p>In 2020, the department is transitioning to a new introductory sequence in the undergraduate curriculum. Among other things, the new sequence changes the order of software courses. Instead of requiring introduction (110) to foundations (401) to data structures (410), the new sequence will require introduction (110) to data structures (210) to foundations (301). In addition, the new foundations course will likely give less weight to object-oriented programming as the preeminent way to structure code, offering a functional programming perspective as well. Both of these changes are ones that I personally championed in an effort to keep our curriculum more in tune with changes that I perceive in the software industry.</p>
<p>This course is a pilot course for the new foundations course (301). However, it's not a strict pilot, since all students who have had data structures (410) have also already had the old version of foundations (401). Therefore, while I do intend to pilot in this course many of the functional programming and other concepts I plan to teach in future offerings of 301, I will also have room to go beyond the normal stopping point of 301 in order to better equip students to design and structure their code.</p>
<p>As an aside, I (or perhaps another instructor) will be in need of LAs for the inaugural 301 offering next semester, and we will primarily be recruiting from the students who have successfully completed this course.</p>
<h3 id="course-description">Course description</h3>
<p>The goal of this course is to teach techniques for structuring larger bodies of code. Simplistic approaches to software design are prone to collapse under their own weight when applied to complex problems. Since students already have experience with object-oriented design, I will instead emphasize functional programming techniques.</p>
<p>We will use the programming language <a href="https://clojure.org">Clojure</a> for all assignments. This is a flavor of the LISP family of languages, which are perhaps best known for their strict parentheses-dominated precedence and prefix notation, but which are also known for their elegance and powerful metaprogramming capability. Clojure runs on the Java Virtual Machine and has access to the entire Java platform, including third-party libraries. I am requiring it not only because of its ability to demonstrate design concepts in a refreshinglydisentangled way (for example, inheritance and polymorphism are separate mechanisms in Clojure), but also because for many Clojure is a distinct advantage in the software industry, as demonstrated by Clojure programmers being among the top 2 or 3 highest paid on average. I will provide a virtual machine with the Clojure language and the Cursive IDE pre-installed.</p>
<p>The course will feature 6 assignments (which build on each other), 4 mid-term exams, and 1 final exam.</p>
<h3 id="learning-objectives">Learning objectives</h3>
<p>At the end of the course, a student successfully completing COMP 590 should be able to:</p>
<ul>
<li>modules, coupling, and cohesion
<ul>
<li>discuss the extent to which an example module demonstrates cohesion</li>
<li>identify points of coupling between modules given the code for one module</li>
<li>discuss the qualities of effective module documentation</li>
</ul></li>
<li>object oriented programming
<ul>
<li>define encapsulation and describe it in an OOP context</li>
<li>describe inheritance in an OOP context and discuss its tradeoffs</li>
</ul></li>
<li>indirection
<ul>
<li>reason about an interface independently of an implementation</li>
<li>describe the benefits and costs of indirection</li>
</ul></li>
<li>design patterns and polymorphism
<ul>
<li>define the observer pattern and describe what problems it solves</li>
<li>define the iterator pattern and describe what problems it solves</li>
<li>identify when to use polymorphism and use it successfully</li>
</ul></li>
<li>functional programming
<ul>
<li>define the difference between state and data</li>
<li>define the difference between a statement and an expression</li>
<li>identify where the effects are given some example code</li>
<li>apply the &quot;functional core, imperative shell&quot; principle to move effects to the outside of some code</li>
</ul></li>
<li>common architectures
<ul>
<li>describe the different responsibilities of the model layer, the controller layer, and the view layer in an MVC paradigm</li>
<li>describe the architecture of a web or mobile app, including the terms frontend, backend, and API</li>
<li>describe the single-effect architecture</li>
<li>discuss the tradeoffs of the single-effect architecture</li>
<li>enumerate actions and parameters in the single-effect architecture given a description of a simple app</li>
</ul></li>
<li>testing
<ul>
<li>discuss how to organize tests for a function or module by behavior</li>
<li>define three types of test double and discuss the tradeoffs of each</li>
<li>discuss the tradeoffs of classical vs. mockist testing</li>
<li>define the 4 phases of testing</li>
<li>compare and contrast state verification, interface verification, and functional verification testing techniques</li>
<li>describe why dependency injection can make testing easier</li>
</ul></li>
<li>asynchrony
<ul>
<li>discuss why asynchronous approaches to code are important</li>
<li>use promises to handle asynchrony in Javascript</li>
</ul></li>
<li>dependencies
<ul>
<li>identify the difference between a library and a framework and discuss associated tradeoffs</li>
<li>discuss the tradeoffs of relying on a dependency</li>
</ul></li>
</ul>
<h3 id="assignments">Assignments</h3>
<p>The assignments have you build a minimal version of the git content tracker. The core of git is surprisingly simple, so I don't expect that this will be overwhelming. Nevertheless, it's important in order to serve the objectives of the course to deal with a problem complex enough that uninformed code design will prove difficult: these are not just programming assignments, but ones designed to make you think carefully about the structure of your code.</p>
<p>Assignments submitted by the deadline will receive a 0% late penalty. Those submitted 2 weeks after the deadline or later will receive a 100% late penalty. Those submitted in between 0 and 2 weeks will receive a penalty between 0% and 100% according to a linear function of lateness; for example, an assignment 1 week late will be penalized 50%. All assignments are absolutely due at the time of the final exam, and no assignments may be submitted for credit afterwards. As no doubt noted in other classes, starting on assignments early is a key determiner of success.</p>
<p>Assignments will be autograded using Gradescope. I encourage you to submit assignments early and often to get feedback about your progress. Only your latest submission will count for grading purposes.</p>
<p>More information is available on the <a href="/assignments/">assignments page</a> of the web site.</p>
<h3 id="grading-criteria">Grading criteria</h3>
<h4 id="letter-grade-boundaries">Letter grade boundaries</h4>
<ul>
<li>A ≥ 95.0</li>
<li>95.0 &gt; A- ≥ 90.0</li>
<li>90.0 &gt; B+ ≥ 87.0</li>
<li>87.0 &gt; B ≥ 83.0</li>
<li>83.0 &gt; B- ≥ 80.0</li>
<li>80.0 &gt; C+ ≥ 77.0</li>
<li>77.0 &gt; C ≥ 73.0</li>
<li>73.0 &gt; C- ≥ 70.0</li>
<li>70.0 &gt; D+ ≥ 67.0</li>
<li>67.0 &gt; D ≥ 60.0</li>
<li>F &lt; 60.0</li>
</ul>
<h4 id="overall-grade">Overall grade</h4>
<ul>
<li>30% is for assignments</li>
<li>60% is for exams</li>
<li>10% is for attendance and participation in class</li>
</ul>
<h4 id="exam-grades">Exam grades</h4>
<p>(Policy adapted from <a href="https://comp110.com/topics/getting-started/syllabus">COMP 110</a>.)</p>
<p>The most fair way to assess mastery of material is through seated quizzes and exams, which account for 60% of your final grade.</p>
<p>The cumulative final exam is worth the full 60% at the start of the semester. Each of the 4 midterm exams you take accounts for 10% of your final grade and reduces the weight of your final examination by 10%. All exams taken count toward your final grade; there are no dropped exams.</p>
<p>For example: by taking all 4 midterm exams, your final exam's weight is 20% of your final grade. If you must be absent from 1 midterm exam (see policy below) then the 3 midterm exams you take will account for 30% of your final grade and your final exam will account for 30%.</p>
<h4 id="midterm-exam-absence-policy">Midterm exam absence policy</h4>
<p>You may be absent for at most 1 midterm exam. To request absence from a quiz, you must submit <a href="https://forms.gle/JPbKoHTJX3ybwVfh7">this form</a> before your absence. Only in the case of medical or otherwise verifiable emergencies can I accept late absence requests.</p>
<p>If you are a part of an organization whose authorized university absences will conflict with more than 1 of the key dates of exams, per course policy you cannot pass this course and should drop the course.</p>
<p>To ensure assessments are fair for all students, and to return graded midterm exams as quickly as possible, hopefully in under 48 hours, we do not offer midterm exam makeups for credit for any reason. By being absent from a midterm exam, the exam's 10% credit will simply not be drawn down from your final exam score's weight. As such, this is not a penalty, it simply means your mastery of the midterm exam's material will instead be assessed on the cumulative final exam.</p>
<h3 id="attendance-and-class-participation">Attendance and class participation</h3>
<p>Attendance is required, and in-class participation accounts for 10% of your overall grade. The form of this participation is not finalized but may include Poll Everywhere polls, think-pair-share sessions, me calling on you in class, etc.</p>
<p>If an illness, family emergency, or other university-recognized reason for missing lecture occurs, you can provide an official letter from the Office of the Dean of Students recognizing the excuse, in which case you will not be penalized for the absence.</p>
<h3 id="lecture-topics">Lecture topics</h3>
<p>Expected lecture topics include:</p>
<ul>
<li>git</li>
<li>Clojure</li>
<li>abstraction</li>
<li>files and modules</li>
<li>cohesion</li>
<li>the single-responsibility principle</li>
<li>automated testing</li>
<li>test doubles</li>
<li>effects</li>
<li>refactoring</li>
<li>dependency injection</li>
<li>coupling</li>
<li>indirection</li>
<li>polymorphism</li>
<li>client/server architecture</li>
<li>single-effect architecture</li>
<li>asynchrony</li>
<li>promises</li>
<li>dependencies</li>
<li>debugging</li>
<li>programming paradigms (FP and OOP)</li>
<li>exceptions</li>
</ul>
<h3 id="deadlines-and-key-dates">Deadlines and key dates</h3>
<ul>
<li>Wed Jan 8: first day of class</li>
<li>Mon Jan 20: no class (Martin Luther King, Jr. Day)</li>
<li>Mon Feb 3: assignment 1 due</li>
<li>Wed Feb 5: midterm exam 1</li>
<li>Wed Feb 17: assignment 2 due</li>
<li>Wed Feb 26: midterm exam 2</li>
<li>Mon Mar 2: assignment 3 due</li>
<li>Mon Mar 9: no class (spring break)</li>
<li>Wed Mar 11: no class (spring break)</li>
<li>Mon Mar 23: assignment 4 due</li>
<li>Wed Mar 25: midterm exam 3</li>
<li>Mon Apr 6: assignment 5 due</li>
<li>Mon Apr 20: midterm exam 4</li>
<li>Fri Apr 24: assignment 6 due (LDOC)</li>
<li>Tue May 5: final exam (12–3pm)</li>
</ul>
<h3 id="classroom-policies">Classroom policies</h3>
<p>You are encouraged to bring your laptop to class to take notes and follow along with the in-class project. However, please be respectful of the instructor and of your fellow students, and don't create a distracting environment by playing games, watching videos, or engaging in other non-class-related activities. I hope that all of you are mature students and that I will not need to belabor this point.</p>
<h3 id="honor-code">Honor code</h3>
<p>(Policy adapted from <a href="https://comp110.com/topics/getting-started/syllabus">COMP 110</a>.)</p>
<p>In order to do well in this course, you must come to your own individual understanding of the material. As such, collaboration is prohibited outside of the following policies.</p>
<p>Make sure that you are familiar with The UNC Honor Code. You will be required to sign an Honor Code pledge to hand in with every exam as well as &quot;sign&quot; the code you submit for grading by filling in your name and ONYEN in the required header. Failing to do so may result in no credit assigned for the problem set.</p>
<h4 id="collaboration-policy-on-general-course-content">Collaboration policy on general course content</h4>
<p>You absolutely may, and are encouraged to, discuss general course concepts (i.e. not assignment-specific) material with anyone, including other current students, tutors, or online communities such as the #clojure channel on the <a href="https://applab.unc.edu">App Lab</a>'s <a href="https://join.slack.com/t/unc-app-lab/signup">Slack group</a>, the <a href="https://ask.clojure.org/">Clojure Q&amp;A site</a>, or #beginners channel on the <a href="http://clojurians.net/">Clojurians Slack group</a>. This includes going over lecture slides, documentation, code examples covered in lecture, study guides, etc. However, the examples you use to discuss general course materials must be from lecture or your own creativity and must not be examples directly drawn from assignments.</p>
<h4 id="collaboration-on-assignments">Collaboration on assignments</h4>
<p>No collaboration with peers inside the course, or anyone outside the course, with the exception of our course LAs while they are working as an LA, is allowed on assignments. Your ability to complete each assignment individually is critical for your ability to do well in this course. Illegal collaboration is easily detected thanks to Stanford's MOSS program (Measures of Software Similarity). Avoiding any fears here is simple: work on assignments on your own and come to office hours when you have questions. Please note that if you know someone who is an LA, you are only permitted to receive help from them while they are working in their official capacity. Receiving help from an LA outside of their working hours is considered unfair advantage for academic gain and is an honor code violation.</p>
<h4 id="permitted-resources-on-assignments">Permitted resources on assignments</h4>
<ul>
<li>materials on the course website and any linked resources</li>
<li>instruction received from UTAs</li>
<li>official Clojure documentation</li>
<li>online documentation for specific errors you encounter</li>
</ul>
<p>The following are not permitted resources on problem sets and worksheets:</p>
<ul>
<li>Talking about specific problems with peers in the course or anyone outside the course with the exception of LAs.</li>
<li>Looking at someone else's screen while working on a problem set or letting someone else look at yours.</li>
<li>Copying any nontrivial amount of code found on any website or community such as StackOverflow, Github, or CourseHero.</li>
<li>Sharing or reusing code with any peer currently in the course or anyone who has previously taken the course.</li>
</ul>
<p>When in doubt, ask Jeff.</p>
<h4 id="tutors-and-informal-help-from-comp-friends">Tutors and informal help from COMP friends</h4>
<p>Tutors and COMP friends are not allowed to help you with problem sets or written worksheets. They may help you with general course material questions and for additional help in preparing for exams, but we encourage you to rely on LA assistance foremost.</p>
<h4 id="code-review-test">Code review test</h4>
<p>Jeff reserves the right to, at any time, ask you to submit to an &quot;code review&quot; test with him or a head LA. We may ask you to meet to explain any line of code or decision made in your program that we deem suspicious or confusing. Thus, you should be able to comfortably explain why you (and you alone) wrote any single line of code in a problem set or response on a worksheet. Should you be unable to do so, your grade will be a zero for the assignment in question and you may be taken to honor court depending on the severity of the infraction.</p>
<h3 id="dealing-with-stress">Dealing with stress</h3>
<p>If you experience unusual levels of stress, whether as a result of this course or not, I encourage you to consider getting help. There's no shame in that. Indeed, I have been the grateful recipient of such help in the past. To get help, you can start by visiting the <a href="https://caps.unc.edu/">UNC Counseling And Psychological Services (CAPS) website</a>.</p>
<h3 id="disclaimer">Disclaimer</h3>
<p>I reserve the right to make changes to the syllabus, including assignment and project due dates, as well as percentages for assignments and exams towards final course grade. These changes will be announced as early as possible and will be reflected on <a href="https://comp590-s20.cs.unc.edu">the course website</a>. If there are discrepancies between a copy of the syllabus and the website, the website is considered the authoritative resource.</p>
      </article>
    </main>

    <footer class="constrained-width">
      <hr>
      <p>Static site generated from <a href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet">Markdown</a> content by <a href="https://pandoc.org/">pandoc</a> and <a href="https://gitlab.com/unc-app-lab/clj-ssg/">custom code</a> written in <a href="https://clojure.org/">Clojure</a>. The <a href="https://gitlab.com/jeff.terrell/comp590-software-arch-website/">site repository</a> is available.</p>
      <p>Site hosted by <a href="https://www.netlify.com/">Netlify</a>.</p>
    </footer>
  </body>
</html>

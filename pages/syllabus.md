---
title: Syllabus
stem: syllabus
---

### Contents

- [Synopsis](#synopsis)
- [General course info](#general-course-info)
- [Instructor](#instructor)
- [Context and target audience](#context-and-target-audience)
- [Course description](#course-description)
- [Learning objectives](#learning-objectives)
- [Assignments](#assignments)
- [Grading criteria](#grading-criteria)
- [Attendance and class participation](#attendance-and-class-participation)
- [Lecture topics](#lecture-topics)
- [Deadlines and key dates](#deadlines-and-key-dates)
- [Classroom policies](#classroom-policies)
- [Honor code](#honor-code)
- [Dealing with stress](#dealing-with-stress)
- [Disclaimer](#disclaimer)

### Synopsis

Techniques to structure larger bodies of code: modules, testing, effects,
refactoring, indirection, polymorphism, asynchrony, common architectures. Uses
the [Clojure](https://clojure.org) programming language.

### General course info

- Term: spring 2019
- Department: Computer Science (COMP)
- Course number: 590: Software Architecture
- Section number: 145
- Time: MW 11:15am &ndash; 12:30pm
- Location: Sitterson 014
- Website: [https://comp590-s20.cs.unc.edu](https://comp590-s20.cs.unc.edu)
- Office hours: see [this page](../office-hours/)
- Textbook: none
- Prerequisite: COMP 410 (data structures)

### Instructor

- Name: Dr. Jeff Terrell
- Email: [terrell@cs.unc.edu](mailto:terrell@cs.unc.edu)
- Office: Sitterson 205
- Web: [http://terrell.web.unc.edu/](http://terrell.web.unc.edu/)

### Context and target audience

In 2020, the department is transitioning to a new introductory sequence in the
undergraduate curriculum. Among other things, the new sequence changes the
order of software courses. Instead of requiring introduction (110) to
foundations (401) to data structures (410), the new sequence will require
introduction (110) to data structures (210) to foundations (301). In addition,
the new foundations course will likely give less weight to object-oriented
programming as the preeminent way to structure code, offering a functional
programming perspective as well. Both of these changes are ones that I
personally championed in an effort to keep our curriculum more in tune with
changes that I perceive in the software industry.

This course is a pilot course for the new foundations course (301). However,
it's not a strict pilot, since all students who have had data structures (410)
have also already had the old version of foundations (401). Therefore, while I
do intend to pilot in this course many of the functional programming and other
concepts I plan to teach in future offerings of 301, I will also have room to
go beyond the normal stopping point of 301 in order to better equip students to
design and structure their code.

As an aside, I (or perhaps another instructor) will be in need of LAs for the
inaugural 301 offering next semester, and we will primarily be recruiting from
the students who have successfully completed this course.

### Course description

The goal of this course is to teach techniques for structuring larger bodies of
code. Simplistic approaches to software design are prone to collapse under
their own weight when applied to complex problems. Since students already have
experience with object-oriented design, I will instead emphasize functional
programming techniques.

We will use the programming language [Clojure](https://clojure.org) for all
assignments. This is a flavor of the LISP family of languages, which are
perhaps best known for their strict parentheses-dominated precedence and prefix
notation, but which are also known for their elegance and powerful
metaprogramming capability. Clojure runs on the Java Virtual Machine and has
access to the entire Java platform, including third-party libraries. I am
requiring it not only because of its ability to demonstrate design concepts in
a refreshinglydisentangled way (for example, inheritance and polymorphism are
separate mechanisms in Clojure), but also because for many Clojure is a
distinct advantage in the software industry, as demonstrated by Clojure
programmers being among the top 2 or 3 highest paid on average. I will provide
a virtual machine with the Clojure language and the Cursive IDE pre-installed.

The course will feature 6 assignments (which build on each other), 4 mid-term
exams, and 1 final exam.

### Learning objectives

At the end of the course, a student successfully completing COMP 590 should be
able to:

- modules, coupling, and cohesion
    - discuss the extent to which an example module demonstrates cohesion
    - identify points of coupling between modules given the code for one module
    - discuss the qualities of effective module documentation
- object oriented programming
    - define encapsulation and describe it in an OOP context
    - describe inheritance in an OOP context and discuss its tradeoffs
- indirection
    - reason about an interface independently of an implementation
    - describe the benefits and costs of indirection
- design patterns and polymorphism
    - define the observer pattern and describe what problems it solves
    - define the iterator pattern and describe what problems it solves
    - identify when to use polymorphism and use it successfully
- functional programming
    - define the difference between state and data
    - define the difference between a statement and an expression
    - identify where the effects are given some example code
    - apply the "functional core, imperative shell" principle to move effects to the outside of some code
- common architectures
    - describe the different responsibilities of the model layer, the controller layer, and the view layer in an MVC paradigm
    - describe the architecture of a web or mobile app, including the terms frontend, backend, and API
    - describe the single-effect architecture
    - discuss the tradeoffs of the single-effect architecture
    - enumerate actions and parameters in the single-effect architecture given a description of a simple app
- testing
    - discuss how to organize tests for a function or module by behavior
    - define three types of test double and discuss the tradeoffs of each
    - discuss the tradeoffs of classical vs. mockist testing
    - define the 4 phases of testing
    - compare and contrast state verification, interface verification, and functional verification testing techniques
    - describe why dependency injection can make testing easier
- asynchrony
    - discuss why asynchronous approaches to code are important
    - use promises to handle asynchrony in Javascript
- dependencies
    - identify the difference between a library and a framework and discuss associated tradeoffs
    - discuss the tradeoffs of relying on a dependency

### Assignments

The assignments have you build a minimal version of the git content tracker.
The core of git is surprisingly simple, so I don't expect that this will be
overwhelming. Nevertheless, it's important in order to serve the objectives of
the course to deal with a problem complex enough that uninformed code design
will prove difficult: these are not just programming assignments, but ones
designed to make you think carefully about the structure of your code.

Assignments submitted by the deadline will receive a 0% late penalty. Those
submitted 2 weeks after the deadline or later will receive a 100% late penalty.
Those submitted in between 0 and 2 weeks will receive a penalty between 0% and
100% according to a linear function of lateness; for example, an assignment 1
week late will be penalized 50%. All assignments are absolutely due at the time
of the final exam, and no assignments may be submitted for credit afterwards.
As no doubt noted in other classes, starting on assignments early is a key
determiner of success.

Assignments will be autograded using Gradescope. I encourage you to submit
assignments early and often to get feedback about your progress. Only your
latest submission will count for grading purposes.

More information is available on the [assignments page](/assignments/) of the
web site.

### Grading criteria

#### Letter grade boundaries

- A &ge; 95.0
- 95.0 &gt; A- &ge; 90.0
- 90.0 &gt; B+ &ge; 87.0
- 87.0 &gt; B &ge; 83.0
- 83.0 &gt; B- &ge; 80.0
- 80.0 &gt; C+ &ge; 77.0
- 77.0 &gt; C &ge; 73.0
- 73.0 &gt; C- &ge; 70.0
- 70.0 &gt; D+ &ge; 67.0
- 67.0 &gt; D &ge; 60.0
- F &lt; 60.0

#### Overall grade

- 30% is for assignments
- 60% is for exams
- 10% is for attendance and participation in class

#### Exam grades

(Policy adapted from [COMP
110](https://comp110.com/topics/getting-started/syllabus).)

The most fair way to assess mastery of material is through seated quizzes and
exams, which account for 60% of your final grade.

The cumulative final exam is worth the full 60% at the start of the semester.
Each of the 4 midterm exams you take accounts for 10% of your final grade and
reduces the weight of your final examination by 10%. All exams taken count
toward your final grade; there are no dropped exams.

For example: by taking all 4 midterm exams, your final exam's weight is 20% of
your final grade. If you must be absent from 1 midterm exam (see policy below)
then the 3 midterm exams you take will account for 30% of your final grade and
your final exam will account for 30%.

#### Midterm exam absence policy

You may be absent for at most 1 midterm exam. To request absence from a quiz,
you must submit [this form](https://forms.gle/JPbKoHTJX3ybwVfh7) before your
absence. Only in the case of medical or otherwise verifiable emergencies can I
accept late absence requests.

If you are a part of an organization whose authorized university absences will
conflict with more than 1 of the key dates of exams, per course policy you
cannot pass this course and should drop the course.

To ensure assessments are fair for all students, and to return graded midterm
exams as quickly as possible, hopefully in under 48 hours, we do not offer
midterm exam makeups for credit for any reason. By being absent from a midterm
exam, the exam's 10% credit will simply not be drawn down from your final exam
score's weight.  As such, this is not a penalty, it simply means your mastery
of the midterm exam's material will instead be assessed on the cumulative final
exam.

### Attendance and class participation

Attendance is required, and in-class participation accounts for 10% of your
overall grade. The form of this participation is not finalized but may include
Poll Everywhere polls, think-pair-share sessions, me calling on you in class,
etc.

If an illness, family emergency, or other university-recognized reason for
missing lecture occurs, you can provide an official letter from the Office of
the Dean of Students recognizing the excuse, in which case you will not be
penalized for the absence.

### Lecture topics

Expected lecture topics include:

- git
- Clojure
- abstraction
- files and modules
- cohesion
- the single-responsibility principle
- automated testing
- test doubles
- effects
- refactoring
- dependency injection
- coupling
- indirection
- polymorphism
- client/server architecture
- single-effect architecture
- asynchrony
- promises
- dependencies
- debugging
- programming paradigms (FP and OOP)
- exceptions

### Deadlines and key dates

- Wed Jan 8: first day of class
- Mon Jan 20: no class (Martin Luther King, Jr. Day)
- Mon Feb 3: assignment 1 due
- Wed Feb 5: midterm exam 1
- Wed Feb 17: assignment 2 due
- Wed Feb 26: midterm exam 2
- Mon Mar 2: assignment 3 due
- Mon Mar 9: no class (spring break)
- Wed Mar 11: no class (spring break)
- Mon Mar 23: assignment 4 due
- Wed Mar 25: midterm exam 3
- Mon Apr 6: assignment 5 due
- Mon Apr 20: midterm exam 4
- Fri Apr 24: assignment 6 due (LDOC)
- Tue May 5: final exam (12&ndash;3pm)

### Classroom policies

You are encouraged to bring your laptop to class to take notes and follow along
with the in-class project. However, please be respectful of the instructor and
of your fellow students, and don't create a distracting environment by playing
games, watching videos, or engaging in other non-class-related activities. I
hope that all of you are mature students and that I will not need to belabor
this point.

### Honor code

(Policy adapted from [COMP
110](https://comp110.com/topics/getting-started/syllabus).)

In order to do well in this course, you must come to your own individual
understanding of the material. As such, collaboration is prohibited outside of
the following policies.

Make sure that you are familiar with The UNC Honor Code. You will be required
to sign an Honor Code pledge to hand in with every exam as well as "sign" the
code you submit for grading by filling in your name and ONYEN in the required
header. Failing to do so may result in no credit assigned for the problem set.

#### Collaboration policy on general course content

You absolutely may, and are encouraged to, discuss general course concepts
(i.e. not assignment-specific) material with anyone, including other current
students, tutors, or online communities such as the #clojure channel on the
[App Lab](https://applab.unc.edu)'s [Slack
group](https://join.slack.com/t/unc-app-lab/signup), the [Clojure Q&A
site](https://ask.clojure.org/), or #beginners channel on the [Clojurians Slack
group](http://clojurians.net/). This includes going over lecture slides,
documentation, code examples covered in lecture, study guides, etc. However,
the examples you use to discuss general course materials must be from lecture
or your own creativity and must not be examples directly drawn from
assignments.

#### Collaboration on assignments

No collaboration with peers inside the course, or anyone outside the course,
with the exception of our course LAs while they are working as an LA, is
allowed on assignments. Your ability to complete each assignment individually
is critical for your ability to do well in this course. Illegal collaboration
is easily detected thanks to Stanford's MOSS program (Measures of Software
Similarity). Avoiding any fears here is simple: work on assignments on your own
and come to office hours when you have questions. Please note that if you know
someone who is an LA, you are only permitted to receive help from them while
they are working in their official capacity. Receiving help from an LA outside
of their working hours is considered unfair advantage for academic gain and is
an honor code violation.

#### Permitted resources on assignments

- materials on the course website and any linked resources
- instruction received from UTAs
- official Clojure documentation
- online documentation for specific errors you encounter

The following are not permitted resources on problem sets and worksheets:

- Talking about specific problems with peers in the course or anyone outside
  the course with the exception of LAs.
- Looking at someone else's screen while working on a problem set or letting
  someone else look at yours.
- Copying any nontrivial amount of code found on any website or community such
  as StackOverflow, Github, or CourseHero.
- Sharing or reusing code with any peer currently in the course or anyone who
  has previously taken the course.

When in doubt, ask Jeff.

#### Tutors and informal help from COMP friends

Tutors and COMP friends are not allowed to help you with problem sets or
written worksheets. They may help you with general course material questions
and for additional help in preparing for exams, but we encourage you to rely on
LA assistance foremost.

#### Code review test

Jeff reserves the right to, at any time, ask you to submit to an "code review"
test with him or a head LA. We may ask you to meet to explain any line of code
or decision made in your program that we deem suspicious or confusing. Thus,
you should be able to comfortably explain why you (and you alone) wrote any
single line of code in a problem set or response on a worksheet. Should you be
unable to do so, your grade will be a zero for the assignment in question and
you may be taken to honor court depending on the severity of the infraction.

### Dealing with stress

If you experience unusual levels of stress, whether as a result of this course
or not, I encourage you to consider getting help.  There's no shame in that.
Indeed, I have been the grateful recipient of such help in the past. To get
help, you can start by visiting the [UNC Counseling And Psychological Services
(CAPS) website](https://caps.unc.edu/).

### Disclaimer

I reserve the right to make changes to the syllabus, including assignment and
project due dates, as well as percentages for assignments and exams towards
final course grade. These changes will be announced as early as possible and
will be reflected on [the course website](https://comp590-s20.cs.unc.edu). If
there are discrepancies between a copy of the syllabus and the website, the
website is considered the authoritative resource.

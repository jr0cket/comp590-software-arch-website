---
title: Notes from Feb 12 lecture
stem: notes-feb-12
---

### Announcements

- music: Lovefool by The Cardigans (1996) - about an insecure lover, which we'll
  come back to...
- (bonus content) learning Clojure sequence functions visually:
  http://blog.josephwilk.net/clojure/functions-explained-through-patterns.html
- assignment 2 announcements
    - A2 autograder released
    - A2 local testing instructions and test suite released
    - bonus opportunities (thank your LAs!)
        - 5% for style, according to cljfmt and clj-kondo (required on future assignments)
        - 5% for early submission (probably will do this again)
- E1 notes
    - Q 2a - quote is actually needed! my bad
    - removed penalties for deps.edn point on exam
    - review reverse/reduce/conj example
- Clem is getting populated, slowly
- Remember that class recordings are available

### Tests vs. incidental complexity

Is writing tests adding incidental complexity?

- it's definitely going above and beyond just getting working software
- how essential is it that you have confidence that your code works?
- how essential is it that you have confidence that you didn't break some existing feature?
- remember: code is for humans. And humans make mistakes. So having a check
  to keep you honest can be helpful.
- so are tests essential? Not strictly, no. But helpful enough that they're
  usually considered worth it.
- I usually don't test for short-lived projects and "spikes" to try things out.
- (But sometimes I find tests helpful even here just to get things developed.)
- But I do usually test for longer-lived projects.
- And definitely test for longer-lived, collaborative projects.

### Why test?

#### You get the perspective of the caller

- this helps keep things taut and minimal
- YAGNI - You Ain't Gonna Need It
- or KISS - Keep It Simple, Stupid
- i.e. stay focused on the actual problem being solved, and don't do extra
  work

#### Tests document developer intentions

- E.g. "I need to be sure this function works on these kinds of inputs"
- Or, "How do I intend this function to be called/this object to be used?"

#### Tests reveal regressions

- Did you break an existing feature?
- if it was tested, you'll know right away
- a test suite is a gift to your future self and any other people working
  on the project

#### Tests close the time gap

- expert programmers create and use tight feedback loops to iterate quickly
- this enables a laser focus
- if something doesn't work, it was in code you just wrote, so you don't
  have to go looking for the cause of a bug
- if something does work, you can move your laser focus onto the next
  small step without feeling like you have to remember details of what you
  just did
- again (this is important): expert programmers create and use tight
  feedback loops to iterate quickly

#### Tests enable refactoring

- get it working, then get it right
- "red, green, refactor"
- the tests (which ideally are fast) give you instant feedback
- make a tiny step, save, and check that tests still pass
- if not, undo the last step to get to green, and try again more carefully
- if so, move on to the next tiny step
- feeling OK about making a messy solution first, because you know you'll
  be able to clean it up later, frees you from a (valid) hangup about
  writing messy code and lets you get into the creative flow
- in other words, decouple getting it /working/ from getting it /right/
- (aside: my Ph.D. co-advisor, Don Smith, gave me advice about writing my
  dissertation that I'll never forget. To overcome writer's block, first
  braindump everything you can think of about a topic. Let it be super
  messy, just get it out of your head. You'll be amazed at how much easier
  it is to work with once it's written down. And he was right.)

#### Testing can be fun

- taking small steps and making steady progress feels good!
- helps you get into a mental state of /creative flow/, which is fun!
- one of the main things that makes programming unfun, IMO, is feeling
  stuck, and tests done well make it hard to get stuck

### Why not test?

#### Tests add complexity to the project

- usually complexity is in a "sidecar" though, tucked away in a separate
  dir, and not getting in the way of understanding and working with the
  code
- tests also can remove complexity by keeping things focused
- I argue that tests are worth the complexity

#### Tests don't catch every bug

But it does catch many of them, and is useful besides.

#### Tests add inertia to the interface

- if you need to change the interface, you also have to change the tests
- this isn't technically "refactoring" at this point, it's a heavier
  change
- something to avoid when possible, but also, if the interface sucks, redo
  it! It just is more work when there are tests that rely on it.

### What to test

- test the code of your project
- don't test functionality of dependencies
- typical setup: 1 test module per code module, 1 test block per code
  function
- also, >= 1 test case per code function

### Testing tutorial

Here's what I did:

- create a new Clojure Deps project in Cursive
- create `spec` and `src` directories
- make `deps.edn` look like this:
    ```
    {:deps {}
     :paths ["src" "classes" "spec"]
     :aliases
     {:test {:extra-deps {speclj
                          {:git/url "https://github.com/kyptin/speclj"
                           :sha "a843b64cc5a015b8484627eff6e84bbac2712692"}}
             :main-opts ["-m" "speclj.cli"]}}}
    ```
- started a terminal window and ran `mkdir classes` then:
   ```
   clojure -R test -e "(do (compile 'speclj.platform.SpecFailure)
                           (compile 'speclj.platform.SpecPending))"
   ```
    - Windows users see
      [note @50 on Piazza](https://piazza.com/class/k58i347dcvy5np?cid=50) for
      details on how to get to a bash-compatible shell, if you don't already
      have one.
- add in the `spec` directory a new Clojure namespace named `test-demo-spec`
- add in the `src` directory a new Clojure namespace named `test-demo`
- in the spec namespace, require `speclj.core`, referring `[describe it
  should=]`
- in the spec namespace, add a simple spec:
    ```
    (describe "the truth"
      (it "is true"
        (should= true true)))
    ```
- then in the terminal window, type `clojure -A:test` to see that one spec
  succeeds
- try the same command with `-f documentation` to see a more detailed listing
  of the specs
- add the `-c` switch for colorized output, and notice that the passing spec is
  printed in green
- run `clojure -A:test -c -a` to start an autotester, which will run the tests
  after any change to the files
- keep the terminal window open so that you can see the latest output in real
  time, as soon as you save a file
- add to the spec namespace a new require for `[test-demo :as sut]`; "sut"
  means "subject under test" and is a common convention in testing code
- remove the vacuous spec and add a new one:
    ```
    (describe "sum"
      (it "returns 0 when given an empty sequence"
        (should= 0 (sut/sum [])))
      (it "returns the only item of a 1-item sequence"
        (should= 7 (sut/sum [7])))
      (it "returns the correct sum of a bunch of numbers"
        (should= 5050 (sut/sum (range 1 101)))))
    ```
- save the file, and note that the spec suite reports all failures.  That's
  good. We're at the "red" step of "red, green, refactor".
- add a `sum` function to the `test-demo` namespace:
    ```
    (def sum (partial reduce + 0))
    ```
- save the file, and notice that we're at green
- add a spec to the `test-demo-spec` namespace for a new function, `mean`:
    ```
    (describe "mean"
      (it "returns nil when given an empty sequence")
      (it "returns the only item of a 1-item sequence")
      (it "returns the correct mean of a bigger sequence"))
    ```
- save the file, and notice that we're in a "yellow" state: our specs have been
  listed, but not actually implemented yet
- I like listing out the behaviors of a function first, before I write the
  tests. This lets me think about behaviors all at once, and it also gives me a
  nice checklist of tiny steps to take, to maintain a tight focus.
- Let's implement the first test case:
    ```
    (it "returns nil when given an empty sequence"
      (should= nil (sut/mean [])))
    ```
- save the file. we're at red.
- define a `mean` function in the `test-demo` namespace:
    ```
    (defn mean [xs])
    ```
- This trivially makes the test suite pass. Let's leave it like that and move
  on. Why not fill out the rest of the `mean` function? Because we're staying
  focused on one tiny step at a time.
- Add the next test case:
    ```
    (it "returns the only item of a 1-item sequence"
      (should= 7 (sut/mean [7])))
    ```
- save the spec file and notice that we're at red
- change the `mean` function to be:
    ```
    (defn mean [xs]
      (when (not-empty xs)
        (first xs)))
    ```
- save the file and we're at green. Again, notice that this isn't a complete
  implementation of `mean`. We're staying focused here. We're also letting the
  tests drive the implementation, so that only the code we truly need is
  written.
- add the next test case:
    ```
    (it "returns the correct mean of a bigger sequence"
      (should= 5050/100 (sut/mean (range 1 101))))
    ```
- save the file; we're at red (notice how nice it feels to stay focused and take tiny steps with fast feedback?)
- change the `mean` function to:
    ```
    (defn mean [xs]
      (when (not-empty xs)
        (/ (sum xs)
           (count xs))))
    ```
- now we're at green. Job finished.
- I was going to move on to stdev to extend the tutorial, but in the interest
  of time, let's move on. I want to give you practice at this.

### Behavior listing quiz

> List a behavior, including the word "it", for the function in this code:
>
> ```
> (defn summation
>   ([xs] (summation identity xs))
>   ([f xs] (reduce + 0 (map f xs))))
> ```

There isn't a right answer, and you'll get better at this with experience, but
here's what I came up with:

- it returns 0 given an empty sequence
- it maps the given fn arg over the sequence before summing
- it sums the items of the mapped sequence

### Testing exercise

- In Cursive, start a new project from this URL:
  https://gitlab.com/jeff.terrell/comp590-testing-exercise
- Follow the setup instructions in the README
- Start a test runner
- Implement the pending tests to get a green test suite


---
title: Notes from March 4 lecture
stem: notes-mar-04
---

### Announcements

- Music: _Never Mind_ by Infected Mushroom&mdash;because this is my jam while
  coding. Highly rhythmic, no words, fast-paced, and fun.
- Many of you are struggling with [A3](../idiot-commit/). I'm learning, thanks
  to your feedback, that I've failed to prepare y'all for that in some
  respects. I'm sorry.  Today, I'm going to try to fix that, with some fun
  lessons along the way.

### Live coding session

Today I'll be live-coding a skeleton of an approach for the write-wtree portion
of A3, since I think that's the part that is tripping most of you up the most.

#### Takeaways

I encourage you to watch the video to get the full experience. These notes will
not do it justice. However, here are the high-level takeaways:

- Java interop
    - Clojure can do anything Java can do, incl. using the JDK
    - Call a method with `(.methodName object arg1 arg2 ,,,)`
    - Can construct an object with e.g. `(File. arg1 arg2)`
    - If referring to e.g. File, need to import it first
- use declare for forward declarations
    - this is creating a var with no value (yet)
    - by the time the symbol is evaluated, the var will have a value
- use records to name concepts
    - you can construct records with `->RecordName` and positional args
    - you can treat records like hashmaps
- use your feedback loops!
    - tests can provide a good feedback loop
    - so can a REPL, especially one integrated with your editor
- functional core, imperative shell
    - collect everything you need into a data structure as the first step
    - transform it as needed
    - then do your effects as the last step

#### Code

And here's the code, with better output printed by `store-entry` than what I
got to in class.

##### `tree_demo_spec.clj`

```clojure
(ns tree-demo-spec
  (:require
   [speclj.core :refer [describe it should=]]
   [tree-demo :as sut]
   [clojure.java.io :as io])
  (:import java.io.File))

(describe "->FileEntry"
  (it "stores info about a file"
    (let [filename "_test-file"
          file (io/file sut/dir filename)
          contents "hello\n"]
      (try
        (spit file contents)
        (let [entry (sut/->FileEntry sut/dir filename)]
          (should= :file (:type entry))
          (should= sut/dir (:parent-path entry))
          (should= filename (:name entry))
          (should= contents (:contents entry)))
        (finally
          (.delete file))))))

(describe "->DirEntry"
  (it "stores info about a tree"
    (let [subdir "_subdir"
          filename "_test-file"
          contents "hello\n"
          parent-dir-file (io/file sut/dir subdir)
          file (io/file parent-dir-file filename)]
      (try
        (io/make-parents file)
        (spit file contents)
        (let [entry (sut/->DirEntry sut/dir subdir)]
          (should= :dir (:type entry))
          (should= sut/dir (:parent-path entry))
          (should= subdir (:name entry))
          (should= [filename] (map :name (:contents entry))))
        (finally
          (.delete file)
          (.delete parent-dir-file)))))

  (it "recurs when an entry is a subdir"
    (let [dir1-name "_subdir1"
          dir2-name "_subdir2"
          filename "_test-file"
          dir1-path (str sut/dir File/separator dir1-name)
          dir2-path (str dir1-path File/separator dir2-name)
          file-path (str dir2-path File/separator filename)
          contents "hello\n"
          file (io/file file-path)]
      (try
        (io/make-parents file)
        (spit file contents)
        (let [entry (sut/->DirEntry sut/dir dir1-name)]
          (let [subdir-entry (first (:contents entry))
                contents (:contents subdir-entry)]
            (should= dir2-name (:name subdir-entry))
            (should= 1 (count contents))
            (should= filename (-> contents first :name))))
        (finally
          (.delete file)
          (.delete (io/file dir2-path))
          (.delete (io/file dir1-path)))))))
```

##### `tree_demo.clj`

```clojure
(ns tree-demo
  (:require [clojure.java.io :as io]
            [clojure.pprint :refer [pprint]]
            [clojure.string :as str])
  (:import java.io.File))

(def dir "test-dir")

(defrecord FileSystemEntry [type parent-path name contents])

(defn ->FileEntry [parent-path name]
  (let [file (io/file parent-path name)]
    (->FileSystemEntry :file parent-path name (slurp file))))

(declare ->Entry)

(defn ->DirEntry [parent-path name]
  (let [file (io/file parent-path name)
        dir-path (str parent-path File/separator name)
        child->entry #(->Entry dir-path %)
        contents (->> file .list (mapv child->entry))]
    (->FileSystemEntry :dir parent-path name contents)))

(defn ->Entry [parent-path name]
  (let [file (io/file parent-path name)]
    (assert (.exists file))
    (if (.isDirectory file)
      (->DirEntry parent-path name)
      (->FileEntry parent-path name))))

(defn remove-subdir [entry subdir-name]
  (letfn [(filter-by-name [entries]
            (filterv #(not= subdir-name (:name %)) entries))]
    (update entry :contents filter-by-name)))

(declare store-entry)

;; Note: each of the following 3 functions, when completed, is expected to
;; return the address of the thing that it saved.

(defn store-blob-entry [{:keys [contents]}]
  (prn 'store-blob-entry contents)
  (format "<addr(%s)>" contents))

(defn store-tree-entry [{:keys [type parent-path name contents]}]
  (let [entries+addresses (mapv (juxt identity store-entry) contents)
        entry->debug-str (fn [[{:keys [name]} addr]] (str name "@" addr))
        entries-str (as-> entries+addresses $
                      (map entry->debug-str $)
                      (apply str $)
                      (str/replace $ #"\n" "\\\\n"))
        dir-debug-str (format "[dir(%s): %s]" name entries-str)]
    (println 'store-tree-entry dir-debug-str)
    dir-debug-str))

(defn store-entry [{:keys [type] :as entry}]
  (if (= type :file)
    (store-blob-entry entry)
    (store-tree-entry entry)))

(comment
  (pprint (->Entry "." dir))
  (pprint (remove-subdir (->Entry "." dir) ".idiot"))
  (store-entry (remove-subdir (->Entry "." dir) ".idiot"))
  )
```

#### Output from `store-entry`

Finally, here's some example output from `store-entry`, when called via the
last line in the `comment` block above. At the call time, the `test-dir`
directory contained (output from `tree -a test-dir`):

```
test-dir
├── .idiot
├── file1
└── subdir
    └── file2
```

And the output is:

```
store-blob-entry "howdy\n"
store-tree-entry [dir(subdir): file2@<addr(howdy\n)>]
store-blob-entry "hello\n"
store-tree-entry [dir(test-dir): subdir@[dir(subdir): file2@<addr(howdy\n)>]file1@<addr(hello\n)>]
```

---
title: Notes from Jan. 22 lecture
stem: notes-jan-22
---

### Announcements

- Music: The Immigrant Song, by Led Zeppelin
    - Like a pack of Viking invaders, Clojure might take over your mind.
- I'm making progress on the autograder, but not there yet.
- Lecture recordings are available on the website.
- Cohorts start next week. Prioritizing schedule-ability over other
  considerations because of relatively small class/LA pool size.
  [Poll to collect schedules](https://forms.gle/PeY1izmCLr9SYU4x7) is open
  today but will close after today.
- LA office hours (in SN141) posted on website and updated to reflect cohort
  times.
- We'll be using My Digital Hand for office hours.
- Remember Piazza for Clojure questions, Cursive questions, etc.
- Goal today: give you everything you need to start on Assignment 1, which is
  due 1 week from Monday.

### Warm-up exercise

> Pair up. Write code that uses `reduce` to find the product of the numbers
> from 5 to 9, inclusive.

```clojure
(reduce * 1 (range 5 10))
```

### 11. more about functions

```clojure
(defn function-name     ; define a new function, like (def function-name (fn ,,,))
  "docstring"           ; displayed when you say (clojure.repl/doc function-name)
  [x y z ,,,]           ; params
  ,,,)                  ; body; returns value of last form

(defn square [x] (* x x))
(square 5)
(apply square [5])      ; apply a seq of arguments to a function (i.e. call it)

(defn plus              ; a "multi-arity" function
  ([] 0)                ; "arity" means "the number of arguments a fn takes"
  ([x] x)
  ([x y] (+ x y)))

(plus)                   ; 0
(plus 5)                 ; 5
(plus 5 4)               ; 9

;; Switching to +, which supports arity > 2.
(apply + (range 10))     ; 45
(apply + 10 (range 10))  ; 55 (apply can take args before the sequence at the end)
(apply + 5 5 (range 10)) ; 55
```

### 12. Clojure CLI

The following assumes that you've properly installed the Clojure CLI tools.

There are 2 CLI commands:

- `clojure` is the basic Clojure runner.
- `clj` is `clojure` plus `rlwrap`, which adds convenience for interactive use.
    - For example, typing `clojure` to get to a REPL, typing a command, then
      hitting the up arrow will produce garbage, not the previous command.
      Keybindings like the up arrow are provided by the [readline
      library](https://tiswww.case.edu/php/chet/readline/rltop.html) via the
      `rlwrap` command.

Use `clojure` when you won't be running a REPL, if you want slightly faster
startup. Or just use `clj` everywhere, because milliseconds don't really
count.

Use the `-m` switch to specify a namespace to run. (For now, think of
namespaces as files.) This calls the `-main` fn of that namespace with the
command-line args as strings. Note: with default settings, all namespaces
should be under the `src/` directory.

Cursive steps:

- Start new IntelliJ project -> Clojure Deps
- In the project tree view, create a new directory in your project named "src".
- Create a new Clojure namespace in the "src" directory. Name it "hello".
- Add (defn -main [] (println "Hello")) to namespace.
- Start a terminal window.
- Change directory to your project. Should be a "src" dir and "deps.edn" file.
- Say `clojure -m hello` to run your `hello/-main` function. This should work.
- Say `clojure -m hellooo` and see what happens.
- Remove the `ns` declaration, say `clojure -m hello` and see what happens.
- Add the `ns` decl, say `clojure -m hello world`, and see what happens. Why?
- How many arguments does `hello/-main` take? 0. But we gave it one ("world").

### 13. Basic destructuring

```clojure
(def v [8 6 7 5 3 0 9])
(let [[v1] v] v1)                ; 8

;; The [] in the name part of the binding means "sequential destructuring".
;; I.e. break apart the sequence and bind names to elements within it.

(let [[v1 v2] v] v2)             ; 6
(let [[v1 v2 v3] v] v3)          ; 7
(let [[v1 v2 v3 & more] v] more) ; '(5 3 0 9)
(let [[v1 v2 & more] v] more)    ; '(7 5 3 0 9)
(let [[v1 & more] v] more)       ; '(6 7 5 3 0 9)
(let [[& more] v] more)          ; '(8 6 7 5 3 0 9)

;; So "&" in a sequential destructuring context means "wrap all the remaining
;; elements in a sequence".

;; This works in the parameter of a function too. Let's fix `hello/-main`:

(defn -main
  [& args]
  (apply println "Hello" args))

;; Now `clojure -m hello world` prints "Hello world".
```

### 14. git interlude: tree and commit objects

Note: I used the [`tree` command](http://mama.indstate.edu/users/ice/tree/) to
print the contents of a directory in a tree style on the command line. This is
a Linux utility that [I installed on my
MacOS](https://superuser.com/questions/359723/mac-os-x-equivalent-of-the-ubuntu-tree-command)
and which Windows users will probably have to install as well.

```
mkdir git-demo
cd git-demo
git init
echo 'Hello world' > hello
git status
git add hello
git status  # now `file` is in the index
git write-tree  # creates a tree object
tree .git/objects
git cat-file -t <each address>  # inspect the type of each object
# there is 1 blob object (a snapshop of the file contents) and 1 tree object
git cat-file -p <each address>  # inspect the contents of eacj object
git commit-tree -m 'First commit' <tree address>
tree .git/objects
git cat-file -t <commit address>
git cat-file -p <commit address>

git reset --hard  # reset your working tree to empty
echo 'Hello world' > dir/hello  # same file, but in a directory now
git add dir/hello
git status  # the move is ready to be committed
git write-tree
tree .git/objects
git commit-tree -m 'Move file' <tree address>
tree .git/objects
# Notice that there are no new blob objects. The address was already taken,
# and it's safe to assume the content is the same.
# Also notice that the tree object containing file 'hello' was reused, for
# the same reason.
```

### Exercise

> Pair up. Write an anonymous function (i.e. using the `fn` form) that takes a
> vector as the only argument, and call it with vector [7 5 9]. Destructure the
> 3 elements of the vector. Return the sum of the second arg and the product of
> the first and third.

```clojure
((fn [[a b c] (+ b (* a c))) [7 5 9])
```

### 15. Miscellaneous, in support of Assignment 1

```clojure
;; Let's support any arity.
(defn -main
  [& args]
  (prn (apply + args)))

;; `clojure -m hello 1 2 3`
;; Error: string cannot be cast to number. Remember: args to -main are strings!

(defn -main
  [& args]
  (prn (apply + (map #(Integer/parseInt %) args))))  ; works

;; `clojure -m hello 1 2 3 four` => NumberFormatException

(defn -main
  [& args]
  (prn (apply + (map #(try
                        (Integer/parseInt %)
                        (catch NumberFormatException e
                          0))
                     args))))  ; prints 6

;; Or try checking the string first with a regex:
(defn -main
  [& args]
  (prn (apply + (map #(Integer/parseInt %)
                     (filter #(re-matches #"\d+" %)
                             args)))))  ; prints 6

;; Note: you are now equipped to do Assignment 1! (But there are more cool
;; things to learn about Clojure.)
```


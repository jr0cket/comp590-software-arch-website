---
title: Notes from Feb 10 lecture
stem: notes-feb-10
---

### Assignment updates

- We'll probably review A1 when we can, but still waiting on some submissions
- Assignment 2 is posted
- Still working on the autograder for A2
- Will release test suite for A2 so you can test locally

### Poll: exam impressions

(This is useful feedback for me.)

> How did you find the exam? (50 responses)
>
> - unreasonably or unfairly hard (1)
> - reasonable, but on the hard side (38)
> - reasonable (10)
> - on the easy side (1)

### Midterm exam 1 review

- Seating assignments were completely random
- Grading was mostly blind: names not visible when actually grading
- Grading was mostly consistent per question
- Curving scores to a B (i.e. 85.0) average
    - only adding to scores, never subtracting
    - linear shift; no fancy Gaussian curving
    - I expect to do this for every exam
- [Exam 1 solutions](https://docs.google.com/document/d/1JZhLK6WFA7wowaJ7giWxfmCbEt5kPK3gI9DNoasoDDc/edit?usp=sharing) walkthrough

### Testing overview

- Tests are a key enabler of making larger programs that don't collapse under
  their own weight.
- (Not all believe testing is the best approach here. A strongly-typed language
  with algebraic data types, like Haskell or OCaml. I don't know this world as
  well so can't speak to it. But probably 90+% of you going to industry will
  deal with tests a lot in your jobs. So testing is the focus here.)
- What do I mean by testing? I mean _automated_ tests, i.e. tests run by the
  computer in a hands-off way.
- Testing is a relatively recent trend. Before automated testing, you had to
  test things manually. Not as enjoyable an experience, and not as reliable.
- Doing automated testing in a good way (as we'll learn) unlocks a
  qualitatively different experience of coding, somewhat like having
  autocomplete in your IDE. Getting instant feedback is great.
- Example of using [speclj](https://github.com/slagyr/speclj) for testing in
  Clojure to test A1:

```clojure
(describe "The program"
  (it "handles the lack of any command-line arguments"
    (should= "invalid input\n" (run ""))))
```

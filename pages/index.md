---
title: Welcome
stem: index
---

Welcome to COMP 590-145: Software Architecture, taught by Professor [Jeff
Terrell](http://terrell.web.unc.edu), which meets on Mondays and Wednesdays
during the spring 2020 semester from 11:15am&ndash;12:30pm in Sitterson 014.

The high-level goal of the course is to learn how to structure code so that the
code to address more complex problems doesn't collapse under its own weight.
This course is a pilot for COMP 301: Foundations of Programming, part of the
new intro sequence, to be offered for the first time in fall of 2020.  The
[syllabus](syllabus/) has more detail about the course.

### Quick links

- [Office hours](../office-hours/)
- [Piazza](https://piazza.com/unc/spring2020/comp590145/home)
- [Poll Everywhere](https://pollev.com/jeffterrell)
- [Feedback for Jeff](https://forms.gle/WsLWbcuZ3urBGZhs6)
- [Midterm exam absence request](https://forms.gle/JPbKoHTJX3ybwVfh7)

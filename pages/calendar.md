---
title: Calendar
stem: calendar
---

Note: this calendar is subject to change.

<div class="calendar-table">

| date       | hw                           | exam  | lecture topics                                                                                                                                                                      |
|------------|------------------------------|-------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Wed Jan 8  |                              |       | (FDOC) course overview; honor code; Clojure essentials ([notes](../notes-jan-8/))                                                                                                   |
| Mon Jan 13 |                              |       | Clojure ([notes](../notes-jan-13/), [video](https://youtu.be/Uo1zkvN_nf8))                                                                                                          |
| Wed Jan 15 |                              |       | Clojure & git ([notes](../notes-jan-15/), [video](https://youtu.be/VNiN4fyP-Xg))                                                                                                    |
| Mon Jan 20 |                              |       | no class: MLK day                                                                                                                                                                   |
| Wed Jan 22 |                              |       | Clojure & git ([notes](../notes-jan-22/), [video](https://youtu.be/xObI1BTFR7Y))                                                                                                    |
| Mon Jan 27 |                              |       | Clojure; abstraction basics ([notes](../notes-jan-27/), [video](https://youtu.be/5lfvexDJPIU))                                                                                      |
| Wed Jan 29 |                              |       | names; modules; cohesion ([notes](../notes-jan-29/), [video](https://youtu.be/PB-7xKauVBQ))                                                                                         |
| Mon Feb 3  | [A1](../clojure-practice/)   |       | margin/review for Exam 1 ([notes](../notes-feb-03/), [video](https://youtu.be/E5VeeATPAP4))                                                                                         |
| Wed Feb 5  |                              | E1    | -                                                                                                                                                                                   |
| Mon Feb 10 |                              |       | post-review Exam 1; testing basics ([notes](../notes-feb-10), [video](https://youtu.be/2ZlTKtnkRWU))                                                                                |
| Wed Feb 12 |                              |       | testing basics ([notes](../notes-feb-12), [video](https://youtu.be/HhrPRnrXQgI))                                                                                                    |
| Mon Feb 17 | [A2](../idiot-init/)         |       | test isolation ([notes](../notes-feb-17), [video](https://youtu.be/59xYPNdHYxs))                                                                                                    |
| Wed Feb 19 |                              |       | effects ([notes](../notes-feb-19))                                                                                                                                                  |
| Mon Feb 24 |                              |       | private functions; refactoring; dependency injection (slides ([HTML](../2020-02-24.refactoring.html), [PDF](../2020-02-24.refactoring.pdf)), [video](https://youtu.be/bYQO7iQZUCM)) |
| Wed Feb 26 |                              | E2    | -                                                                                                                                                                                   |
| Mon Mar 2  |                              |       | post-review Exam 2; coupling and indirection (slides ([HTML](../2020-03-02.coupling.html), [PDF](../2020-03-02.coupling.pdf)), [video](https://youtu.be/R4atKtA9ups))               |
| Wed Mar 4  | [A3](../idiot-commit/) (Thu) |       | live-coding session for A3 concepts ([notes](../notes-mar-04), [video](https://youtu.be/AsBX-cqMko8))                                                                               |
| Mon Mar 9  |                              |       | no class: spring break                                                                                                                                                              |
| Wed Mar 11 |                              |       | no class: spring break                                                                                                                                                              |
| Mon Mar 16 |                              |       | git refs (for A4); polymorphism and interfaces                                                                                                                                      |
| Wed Mar 18 |                              |       | client/server architecture                                                                                                                                                          |
| Mon Mar 23 | [A4](../idiot-switch/)       |       | A5 info; single-effect architecture                                                                                                                                                 |
| Wed Mar 25 |                              | E3    | -                                                                                                                                                                                   |
| Mon Mar 30 |                              |       | post-review Exam 3; asynchrony and promises                                                                                                                                         |
| Wed Apr 1  |                              |       | dependencies; debugging                                                                                                                                                             |
| Mon Apr 6  | A5                           |       | A6 info; programming paradigms: OOP and FP                                                                                                                                          |
| Wed Apr 8  |                              |       | exceptions; other architectures (TBD)                                                                                                                                               |
| Mon Apr 13 |                              |       | other architectures (TBD)                                                                                                                                                           |
| Wed Apr 15 |                              |       | commenting best practices                                                                                                                                                           |
| Mon Apr 20 |                              | E4    | -                                                                                                                                                                                   |
| Wed Apr 22 |                              |       | post-review Exam 4 and review for final                                                                                                                                             |
| Fri Apr 24 | A6                           |       | (classes end)                                                                                                                                                                       |
| Tue May 5  | -                            | final | final exam, 12-3pm                                                                                                                                                                  |

</div>

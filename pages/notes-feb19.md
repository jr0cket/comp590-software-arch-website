---
title: Notes from Feb 19 lecture
stem: notes-feb-19
---

### Announcements

- A1 solution posted (see [Announcements](../blog/))
- [A3](../idiot-commit/) posted (no autograder yet)
- notes typically get filled out after lecture
- [website
  repo](https://gitlab.com/jeff.terrell/comp590-software-arch-website/) lists
  content changes (see footer of each page for a link)
- Exam 2 in a week!

### Clarifications from last time

- all a stub is is a dummy function that keeps track of its invocations and by
  default returns `nil`
- check out [the Speclj API reference](http://micahmartin.com/speclj/) to
  understand what the possibilities are, e.g. what options the `stub` function
  supports
- to get started with Speclj, see "local testing" hint at bottom of
  [A2](../idiot-init/)
- a little more on
  [`with-redefs`](https://clojuredocs.org/clojure.core/with-redefs)
    - it's a macro so behaves more like `let` than like a normal function call
    - like `let`, it takes 2+ params: a vector of bindings (1st param) and a
      body (all the rest)
    - like `let`, each binding is a pair of name and value
    - we're changing what each name points to during evaluation of the body
- a recipe for creating isolated tests
    1. determine what functions the function-under-test (FUT) needs to call,
       and how it will call them
    2. redefine the called fns with a `with-redefs` block to refer to stubs
    3. call the FUT
    4. make assertions about how the stubs were invoked or were not invoked

### The test isolation spectrum

First, some definitions.

- A _unit test_ is one that tests only a single unit. "Unit" here can mean
  function, class, module, etc. Typically when I use it I mean function.
    - Example: the `sum` tests from last lecture, or the isolated `mean` and
      `stdev` tests.
- An _integration test_ is one that tests more than one unit and the
  interactions between them. It's testing the _integration_ of the units, i.e.
  that they work correctly together.
    - Example: the unisolated `mean` test, which is testing the integration of
      the `sum` unit and the `mean` unit.

Integration tests vary in size.

- The unisolated `mean` test only tested 2 (small) units.
- At the other extreme, "end-to-end" (E2E) or "system" tests test the
  integration of everything, all together, and typically treat your entire
  program as a black box, using it more or less from the perspective of the
  actual user.
    - Example: the autograders that I'm writing for your assignments are doing
      an E2E or system test. I don't want to dictate a structure to you about
      how to organize your code, so I have to test things this way.
    - Example: in my startup, we wrote a backend API and tested it from an
      external perspective. The tests just needed to be told what the API URL
      was, and they'd test the API by actually issuing API requests and
      asserting things about the responses.
    - Example: a web app can be tested by driving a browser to do certain
      things and asserting things about the page or browser actions. Cypress.io
      is a modern tool that does this. It actually fires up a browser, points
      it at your web app, and interacts with the page according to a script
      that you write.

The test isolation spectrum:

- So at the small end of the spectrum are unit tests, which are fully isolated.
- At the big end of the spectrum are E2E or system tests, which are fully
  integrated.
- In between are various sizes of integration tests, which integrate more or
  fewer units together.

Pros and cons:

- unit tests:
    - tend to be faster, which is important for feedback loops, as we discussed
    - avoid a combinatorial explosion: test variations of a unit, not the
      variations of a combination of units
    - for example: if you have a small integration test involving 2 units, the
      first of which can vary 3 ways and the second can vary 4 ways, if you
      want to fully cover the variations you need 12 (= 3 * 4) tests. But if
      you test each unit in isolation, you need 7 (= 3 + 4) tests. This problem
      gets worse with big tests with lots of units, even if each unit only has
      1-2 variations.
- benefits of E2E tests:
    - good "bang for the buck": cover lots of code with a single test
    - can catch bugs that isolated unit tests don't, if the bugs are due to
      using an interface incorrectly
    - the idea here is that, if your tests are fully isolated, only the
      functions themselves are being tested, and bugs can hide in the crevices
      between functions, where one function calls another, because one unit
      never really calls another in fully isolated tests
    - example: if you rename `mean` to `average` but forget to update the body
      of `stdev` to use the new name, isolated unit tests still pass even
      though the code is broken!
    - (This is really more about unit vs. integration tests than it is about
      unit vs. E2E tests.)

Recommendations

- make most of your tests unit tests
- or maybe not fully isolated with stubs and all, but at least integrating a
  very small number of units
- if you need to have good confidence that your app really works, consider
  adding an E2E "smoke" test, that just exercises the "happy path" (i.e. the
  well-trod, non-exceptional path) to ensure everything is connected correctly
    - this is especially useful in a continuous deployment (CD) context, where
      commits are automatically deployed if they pass the tests
- *don't* try to test everything in an E2E way--it's awful, and I would
  know
    - I worked on an app once that tried to do this, and it was awful. Long
      feedback cycles, brittle and flaky tests, etc. It was hard to get
      anything done.

### What's wrong with this code?

```clojure
(ns main
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:import (java.io BufferedReader BufferedWriter)))

(defn print-table-row [csv-line]
  (print "<tr>")
  (doseq [field (str/split csv-line #",")]
    (print "<td>")
    (print field)
    (print "</td>"))
  (print "</tr>"))

(defn -main [& args]
  (let [[in-path out-path] args]
    (with-open [input (-> in-path io/file io/reader BufferedReader.)
                output (-> out-path io/file io/writer BufferedWriter.)]
      (binding [*out* output]
        (loop [header-seen? false]
          (when-let [line (.readLine input)]
            (if-not header-seen?
              (do
                (print "<!DOCTYPE html><html lang=\"en\"><body><table><thead>")
                (print-table-row line)
                (print "</thead><tbody>")
                (recur true))
              (do
                (print-table-row line)
                (recur true)))))
        (print "</tbody></table></body></html>\n")))))

(ns main-spec
  (:require [speclj.core :refer [describe it should=]]
            [main2 :as sut]))

(describe "the main program"
  (it "converts CSV content from first arg to an HTML table at second arg"
    (let [in-path "input.csv"
          out-path "output.html"]
      (spit in-path (str "Name,Species,Age\n"
                         "Greta,canine,8\n"
                         "Jupiter,feline,2\n"
                         "Annie,goat,4\n"
                         "Adelaide,goat,4\n"))
      (sut/-main in-path out-path)
      (should= (str "<!DOCTYPE html>"
                    "<html lang=\"en\"><body><table><thead>"
                    "<tr><td>Name</td><td>Species</td><td>Age</td></tr>"
                    "</thead><tbody>"
                    "<tr><td>Greta</td><td>canine</td><td>8</td></tr>"
                    "<tr><td>Jupiter</td><td>feline</td><td>2</td></tr>"
                    "<tr><td>Annie</td><td>goat</td><td>4</td></tr>"
                    "<tr><td>Adelaide</td><td>goat</td><td>4</td></tr>"
                    "</tbody></table></body></html>\n")
               (slurp out-path)))))
```

- it's not wrong...it passes the tests
- problem: it's affecting the filesystem
- what if `input.csv` or `output.html` were important apart from the testing
  context? Too bad, they get clobbered!
- we could be more defensive: come up with names unlikely to conflict, put
  them in an oddly named directory that we remember to delete afterwards
- but all of this is getting complicated
- we could stub out `print`, but what a mess to keep track of all those
  calls, and what if `println` or `prn` are used instead?
- my thesis: uncareful use of _effects_ is usually what makes code difficult
  to test
- other examples of things that are hard to test:
    - a fn that writes a record to the database
    - a fn that sends an HTTP request
    - a fn that writes a file to the `.idiot/objects/` directory

### What's an effect?

An effect is basically anything _destructive_:

- to the screen, e.g. `print` statements
- to the contents of a file
- to the document of a web page
- to a row in the database
- to the _value_ of a _variable_

But note that, while in some ways it might be nice to pursue a sort of monastic
approach to coding without effects, _effects are necessary_ to actually getting
work done. If your program doesn't have any effects, there's no point in
actually running it. So the goal isn't to avoid effects entirely, but to be
careful where and how we use them in our code.

Note that there's a technical difference here between _statements_ and
_expressions_. Expressions tend to be evaluated for their return value, or just
value. Statements tend to be evaluated for their effect. For example, in a
language with an `if` statement, the statement never returns a value; that's a
meaningless concept. If you want a conditional that actually returns a value,
you have to use the so-called "ternary operator" (i.e. the thing with the `?`
and `:`) instead to make a conditional expression.

Also note that other languages are thoroughly oriented around the idea of
statements. Clojure tends to be far more oriented around expressions than
statements, although statements are still present (e.g. `println`).

### Exercise: identify effects in code

Each prompt had 3 answers:

1. effectful
2. not effectful
3. it depends

Code snippets:

1. `(let [squares (map #(* % %) (range 5))] ,,,)`

    Answer: not effectful. We're not changing anything here, just dealing with
    values.

2. `(def sum (partial reduce + 0))`

    Answer: effectful. By calling `def`, we're modifying the current namespace
    by adding or updating a var.

3. `(aset (long-array [3 6 9]) 2 12) ; assign a new value to array at index 2`

    Answer: effectful. We're changing an element in a Java array. Any mutation
    is an effect.

4. `(let [pt (java.awt.Point. 1 2)] (set! (.-x pt) 8) pt) ; assign a new value to object at field .-x`

    Answer: effectful. We're changing the value of a field of a Java object,
    which is a mutation.

5. `(some-function 1 2 3)`

    Answer: it depends. If `some-function` is effectful, the function call will
    be also, and vice-versa.

### How should we deal with effects?

Initially, don't worry about them. Remember "red, green, refactor"? Dealing
with effects should be something you worry about after you get your code
working.

Right now, it's work to recognize effects, and it will be work to figure out
what to do with them, if anything. But in time, this will get easier, as you
learn "effects mindfulness".

#### Strategy 1: move effects to the beginning and the end

In the example above, part of the challenge is that the `print` statements are
scattered throughout the code.

If we instead read the input file completely with `slurp`, did our work using
the string of the file contents, then only wrote the output at the very end
with `spit`, we'd go from like a dozen effects (each of the print statements)
down to 2, one at the very beginning and one at the very end. This is the key
change we'll make to the example code in a bit.

A business term that applies here is "extract, transform, load", or ETL. The
idea is that you fetch the data you need, transform it locally, then go store
the results somewhere. That tends to be a good approach to managing effects
better.

#### Strategy 2: sanctify thyself

Somebody has to do the dirty work of making these effects happen. If you have a
function that does some of this dirty work, try making the caller do the dirty
work instead.

Typically this will mean that the effects have to happen before or after the
function gets called, but that's not necessarily true. A function could accept
a function parameter that might or might not contain some effects, and it could
call that function as necessary. For example, a function might have a parameter
called `when-given-array-is-too-long`. That function might have effects and it
might not. Whether it does it up to the caller, not the function itself. If the
function itself does not have any effects, then it's sanctified itself.

This approach is pretty compatible with the idea of moving the effects to the
beginning and end (see Strategy 1). In general, as you move effects up the call
stack, they tend to float to the beginning and end, which we'll see below when
we talk about "functional core, imperative shell".

#### Strategy 3: use _pure_ functions

What's a "pure" function? It's a function:

1. that has no effects and
2. that has everything it needs from its parameters.

The major consequence of making a function pure is that it is _deterministic_;
that is, it always returns the same value given the same inputs.

This is huge. We can take this function out of the stream of time. It no longer
depends on any ordering of operations. It _always_ returns the same value given
the same inputs. Therefore, it's much easier to wrap one's head around, which
makes it easier to reason about, compose, extend, and use.

Also, pure functions are dead simple to test. You call it with certain
arguments and assert that it returned the value you expect. Couldn't be easier.

2 classic sources of impurity are I/O (input or output) and randomness. I/O
means you depend on the value read in (e.g. from a file or from the database)
_at that point in time_, which might change the next time you run it.  Getting
a random number means that you're relying on state that is stored in the
pseudorandom number generator, and your returned value probably isn't
deterministic at that point.

A pure function can be described by the following acronym (which should be pronounced in an Italian accent): **VIVONE**; that is:

- **V**alues **I**n
- **V**alues **O**ut
- **N**o **E**ffects

What's a value? In Clojure, most things:

- any data:
    - primitive data types like numbers and strings
    - collections of data, e.g. vectors, hashmaps
- functions
    - e.g. for higher-order functions like `map` and `reduce` that receive
      functions as arguments

### Goal: functional core, imperative shell (Bernhardt)

At the end of the day, what we're going for is, in the words of Gary Bernhardt,
a _functional core, imperative shell_ (FCIS). That is, most of the meat of your
program should be pure functions. The effects should all live at the "outside"
of your program, in the "shell"; that is, at the beginning, end, or edges of
your program. The picture here is that of an M&M, with its chocolate center and
thin candy shell.

### Cleaned up example code

After applying these strategies, here's what I came up with for a program with
better effects management than what I had above:

```clojure
(ns main2
  (:require [clojure.string :as str]))

(defn wrap [tag inner]
  (format "<%s>%s</%s>" tag inner tag))

(defn csv-line->tr
  [csv-line]
  (let [split-fields #(str/split % #",")
        wrap-td (partial wrap "td")]
    (->> csv-line
         split-fields
         (map wrap-td)
         (apply str)
         (wrap "tr"))))

(defn csv->html [csv-data]
  (let [[header-line & data-lines] (str/split csv-data #"\n")
        wrap-html #(str "<!DOCTYPE html><html lang=\"en\">" % "</html>\n")
        thead (wrap "thead" (csv-line->tr header-line))
        tbody (wrap "tbody" (apply str (map csv-line->tr data-lines)))
        table (wrap "table" (str thead tbody))]
    (wrap-html (wrap "body" table))))

(defn -main [& args]
  (let [[in-path out-path] args]
    (->> (slurp in-path)
         csv->html
         (spit out-path))))
```

The imperative shell is contained in `-main`, which contains all the I/O
necessary. Everything else is functional core.

This is far easier to test, because I can test the pure functions easily.

In fact, I don't even need to test `-main`, because it's so simple. I can just
test the functional core, not test the imperative shell, and still be confident
that the program is going to work as intended.

### Exercise: which functions are pure?

The answers for each question were:

1. pure
2. impure
3. it depends

```clojure
(defn square [x]
  (* x x))
```

Pure. (The fact that we're saying `defn` is indeed an effect on the namespace,
but the function itself is pure.)

```clojure
(defn lookup-person [id]
  (get people-by-id id))
```

Impure. We are referring to an external value that wasn't passed in as an
argument.

```clojure
(defn sum-of-squares [x y]
  (prn 'x x 'y y)
  (+ (* x x) (* y y)))
```

Impure. We're doing I/O by calling `prn`.

```clojure
(doseq [item sequence]
  (process-item item))
```

Probably impure. Anytime you need to call `doseq`, it's because there's some
effect that's important to have happen. Otherwise, you could say `for` or
`map` and be lazy.

```clojure
(defn process-item [item]
  (register-item item)
  (item->map item))
```

Probably impure. We're not using the returned value from the call to
`register-item`, which is a good clue that we're calling it for its effect
rather than its value.

---
title: Notes from Jan. 8 lecture
stem: notes-jan-8
---

- Reviewed the [syllabus](../syllabus/)
- LA office hours and location TBD
- Assignment 1 will be written ASAP
- Why Clojure?
    - It's simpler, in a technical sense: concepts are not as entangled
    - I will be better able to teach concepts when they're disentangled
    - I love the language
    - It pays very well, probably because you get work done faster in it
- Clojure basics
    - It comes from the LISP family of languages, which feature prefix notation
      (operator before operands) and lots of parenthesis (although not actually
      much more than other languages).
    - numbers: `1`, `1.0`
    - `"strings"`
    - `:keywords`
    - vectors: `[1 2 3]`
    - hashmaps: `{:name "Jeff", :age 38}`
    - commas are whitespace! So this works too: `{:name,"Jeff" :age 38}`. (But
      don't do that.)
    - comments start with `;`
    - sets: `#{1 2 3}  ; no duplicate elements`
    - arbitrary nestability, e.g.
      `{:jeff {:name "Jeff", :age 38, :foo [1 2 [3 #{4}]]}}`
    - function calls: `(function arg1 arg2)`
    - e.g. `(inc 7)` and `(range 10)`
    - lists: `(list 1 2 3)`
    - laziness is possible with lazy sequences, e.g. `(range)`, which evaluates
      to an infinite sequence

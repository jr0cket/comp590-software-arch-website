---
title: Assignment 2: Idiot Init
stem: idiot-init
---

### Due

The last second of Monday, February 17<sup>th</sup>. Late submissions accepted
until the last second of Monday, March 2<sup>nd</sup> (see
[syllabus](../syllabus/#assignments)).

### Bonus opportunities

(Thank your LAs for persuading me to offer these bonus opportunities. :-) )

If your latest submission is at or before the last second of Friday, February
14<sup>th</sup> will receive a 5% bonus to the autograded grade.

There is also 5% bonus possible from style points. The autograder checks this
automatically. I couldn't quickly figure out how to configure the autograder on
Gradescope to allow bonus points beyond the "max" score, so the assignment has
44 points possible including the style points, but it will actually be graded
out of 40 points. (Note: style will be a required part of future assignments.)

### Context

Git calls itself "the stupid content tracker". Building on that theme, let's
build a git clone called idiot.

This assignment will be the foundation that later assignments are built on. The
next assignment will assume that this one is finished. So it might be worth
spending some time structuring your code well before moving on.

### Requirements

Make a Clojure program named `idiot`, invocable on the command line with
`clojure -m idiot`.  Define 4 commands, each detailed below. If invoked with an
unrecognized command, print `"Error: invalid command\n"`. If invoked with no
further arguments, with a `-h` argument, or with a `--help` argument, print the
following usage message:

```
idiot: the other stupid content tracker

Usage: idiot <command> [<args>]

Commands:
   help
   init
   hash-object [-w] <file>
   cat-file -p <address>
```

#### Command 1: help

If `idiot` is invoked with `help` as the first argument, the help command is
invoked. If no further arguments are given, print the top-level usage given
above. If an argument of `-h` or `--help` is given, print the following usage
message:

```
idiot help: print help for a command

Usage: idiot help <command>

Arguments:
   <command>   the command to print help for

Commands:
   help
   init
   hash-object [-w] <file>
   cat-file -p <address>
```

If an unknown command is given, print `"Error: invalid command\n"`.

If a known command is given (including `help`), print the usage message for
that command. See below for the usage messages. Tolerate and ignore extra
arguments after the command name.

#### Command 2: init

If `idiot` is invoked with `init` as the first argument, the init command is
invoked. If a further argument of `-h` or `--help` is given, print the
following usage message:

```
idiot init: initialize a new database

Usage: idiot init

Arguments:
   -h   print this message
```

If any arguments other than `-h` or `--help` are given, print `"Error: init
accepts no arguments\n"`.

Otherwise, if the `.git` directory exists in the current directory, print
`"Error: .git directory already exists\n"`.

Otherwise, create a `.git` directory in the current directory, create an
`objects` directory inside the `.git` directory, and print `"Initialized empty
Idiot repository in .git directory\n"`.

#### Command 3: hash-object

If `idiot` is invoked with `hash-object` as the first argument, the hash-object
command is invoked. If a further argument of `-h` or `--help` is given, print
the following usage message:

```
idiot hash-object: compute address and maybe create blob from file

Usage: idiot hash-object [-w] <file>

Arguments:
   -h       print this message
   -w       write the file to database as a blob object
   <file>   the file
```

Otherwise, if the command is run outside of a repository (i.e. if the `.git`
directory does not exist in the current directory), print
<code>"Error: could not find database. (Did you run \`idiot init\`?)\n"</code>.

Otherwise, if no file argument is given, print `"Error: you must specify a
file.\n"`.

Otherwise, if the file argument is given but can't be read (perhaps because it
does not exist), print `"Error: that file isn't readable\n"`.

Otherwise, print the computed address of the contents of the given file
followed by a newline, and, if the `-w` switch is given, also save the contents
of the file as a blob object in the idiot database. Details of these operations
follow.

##### Blob addressing and storage details

First, some context. A blob is the simplest type of object that we will store
in our content-addressable object database, and the only one we will store for
this assignment. A blob is simply a chunk of data, a sequence of bytes. A
filesystem stores such chunks as files, using the full path (parent directories
and filename) as the index to look up a stored chunk. Git (and idiot) stores
such chunks as blobs, using the content-derived address as the index to look up
a stored chunk.

Both the address and actual data of a blob include a header with some metadata
about the blob. Let's call this the `header+blob` for purposes of this
description. For example, if a blob is `"something\n"`, the `header+blob` would
be (in a Clojure string):

    "blob 10\000something\n"

Let's break this down. `blob` is the object type. We'll see other object types
in future assignments. Then comes a space. Then the blob length, as a string.
Then a "NUL" character, i.e. with character code 0. Finally, the blob itself.
As another example, a blob of "Hello there." would have a `header+blob` of:

    "blob 12\000Hello there."

The address of a blob is computed as the SHA1 checksum of the `header+blob` of
that blob. Here is Clojure code to return an address given a `header+blob`:

```clojure
;; Note: you must import java.security.MessageDigest for this to work.
;; E.g. include `(:import java.security.MessageDigest)` in your `ns` form.

(defn sha1-hash-bytes [data]
  (.digest (MessageDigest/getInstance "sha1")
           (.getBytes data)))

(defn byte->hex-digits [byte]
  (format "%02x"
          (bit-and 0xff byte)))

(defn bytes->hex-string [bytes]
  (->> bytes
       (map byte->hex-digits)
       (apply str)))

(defn sha1-sum [header+blob]
  (bytes->hex-string (sha1-hash-bytes header+blob)))
```

The blob is stored in the `.git/objects` directory using this address, with two
wrinkles. First, the address is split after 2 characters to add 1 level of
hierarchy to the object database. (This is for performance reasons: filesystem
performance degrades when there are too many files in a directory.) For
example, a blob with computed address of
`deba01fc8d98200761c46eb139f11ac244cf6eb5` will be stored in the file
`.git/objects/de/ba01fc8d98200761c46eb139f11ac244cf6eb5`. (Note the `/` after
`de`.)

The second wrinkle is that the content is zipped using the standard zlib
process. Here is some Clojure code to zip some data and return a
`ByteArrayInputStream` object of the zipped content:

```clojure
;; Note: this code assumes that `clojure.java.io` has been required as `io`.
;; E.g. include `(:require [clojure.java.io :as io])` to your `ns` form. It
;; also assumes that the `java.util.zip.DeflaterOutputStream`,
;; `java.io.ByteArrayOutputStream`, and `java.io.ByteArrayInputStream` classes
;; have been imported, e.g. by adding this statement to your `ns` form:
;; `(:import java.util.zip.DeflaterOutputStream
;;           (java.io ByteArrayInputStream
;;                    ByteArrayOutputStream))`

(defn zip-str
  "Zip the given data with zlib. Return a ByteArrayInputStream of the zipped
  content."
  [data]
  (let [out (ByteArrayOutputStream.)
        zipper (DeflaterOutputStream. out)]
    (io/copy data zipper)
    (.close zipper)
    (ByteArrayInputStream. (.toByteArray out))))
```

The returned value can then be saved to a file like so (assuming the same `io`
require as above):

```
(io/copy (zip-str header+blob) (io/file path-of-destination-file))
```

As an example, here is a hex dump (using the `hexdump` utility) of the blob
`"something\n"`, stored along with its header at
`.git/objects/de/ba01fc8d98200761c46eb139f11ac244cf6eb5`:

```
0000000 78 9c 4b ca c9 4f 52 30 34 60 28 ce cf 4d 2d c9
0000010 c8 cc 4b e7 02 00 38 63 05 f9
000001a
```

(If you actually call `git hash-object -w` yourself for testing purposes, note
that the file that git saves might be very slightly different than the file
`idiot` might save given the above code. When I tested this, the difference
proved to be inconsequential: I could drop my generated file into the
`.git/objects` database and `git cat-file -p` could read it just fine. My guess
is that the differing bytes have to do with specifics about the zlib
compression.)

#### Command 4: cat-file

If `idiot` is invoked with `cat-file` as the first argument, the cat-file
command is invoked. If a further argument of `-h` or `--help` is given, print
the following usage message:

```
idiot cat-file: print information about an object

Usage: idiot cat-file -p <address>

Arguments:
   -h          print this message
   -p          pretty-print contents based on object type
   <address>   the SHA1-based address of the object
```

Otherwise, if the command is run outside of a repository (i.e. if the `.git`
directory does not exist in the current directory), print
<code>"Error: could not find database. (Did you run \`idiot init\`?)\n"</code>.

Otherwise, if no `-p` switch is given, print `"Error: the -p switch is
required\n"`.

Otherwise, if no address argument is given, print `"Error: you must specify an
address.\n"`.

Otherwise, if an address argument is given but an object at the given address
could not be found, print `"Error: that address doesn't exist\n"`.

Otherwise, print the contents of the blob (not including the header) stored at
the given address in the object database. That is, read the `header+blob`
information from the database, but only print the blob portion, without the
header.

To read zipped content from the file, try this code, courtesy of Ben Knoble and
Amy Clark:

```clojure
;; Note: this code assumes that `clojure.java.io` has been required as `io`
;; and that the java.io.ByteArrayOutputStream and
;; java.util.zip.InflaterInputStream classes have been imported.
;; For example:
;; (ns ,,,  ; your namespace name goes here
;;   (:require [clojure.java.io :as io])
;;   (:import java.io.ByteArrayOutputStream
;;            java.util.zip.InflaterInputStream))

(defn unzip
  "Unzip the given data with zlib. Pass an opened input stream as the arg. The
  caller should close the stream afterwards."
  [input-stream]
  (with-open [unzipper (InflaterInputStream. input-stream)
              out (ByteArrayOutputStream.)]
    (io/copy unzipper out)
    (->> (.toByteArray out)
      (map char)
      (apply str))))
```

Then, if `clojure.java.io` is required and aliased as `io` in the current
namespace, you can use this code to unzip a file named `"blob"`:

```clojure
(with-open [input (-> "blob" io/file io/input-stream)]
  (unzip input))
```

### Submission instructions

You will need to submit your code to Gradescope as a Clojure deps project. What
this means is that the top-level directory of the repository or .zip file that
you submit to Gradescope should have a `src` directory with at least an
`idiot.clj` file in it as well as a `deps.edn` file whose contents should be at
least `{}`. If your submission is missing these elements, or if they're not in
the top-level (root) directory, then the autograder will not work.

Update: as of Tuesday, February 11, 11pm, the autograder is set up. Post on
Piazza if you notice anything strange.

### Hints

- To print the blob length for the `blob+header`, given the length as a number,
  you can convert it to a string with `str` or by using `format` with a `%d`
  specifier.
- You can call the `.mkdirs` method on a `File` object to make directories. For
  example, with the `clojure.java.io` namespace aliased as `io`, saying
  `(.mkdirs (io/file "parent/child"))` will create the `parent` directory and
  also the `child` directory inside it.

#### Running tests locally

The autograder takes a while to run. For each test case, I'm starting up a JVM
to run your Clojure code in a separate process. Personally, such long feedback
loops drive me crazy. For the autograder's test suite and setup instructions,
which will enable much faster feedback loops for your coding pleasure, keep
reading. It's a little involved (sorry)&emdash;`speclj` isn't maintained
anymore, and I haven't had time to make things easier.

1. Download [idiot_spec.clj](../idiot_spec.clj) and
   [idiot_spec_utils.clj](../idiot_spec_utils.clj) and put them in the `spec`
   directory of your project (creating the directory if need be).

2. Edit your `deps.edn` file to add `:paths ["src" "classes" "spec"]` to the
   deps map. If you don't have a `deps.edn`, create one with the path key and
   value wrapped in braces, e.g. `{:paths ["src" "classes" "spec"]}`.

3. Add a test alias to your `deps.edn` file:

    ```clojure
    {
     ;; leave :paths and anything else alone
     :aliases
     {:test {:extra-deps {speclj
                          {:git/url "https://github.com/kyptin/speclj"
                           :sha "a843b64cc5a015b8484627eff6e84bbac2712692"}}
             :main-opts ["-m" "speclj.cli"]}}
    }
    ```

4. Run the following commands in a bash compatible terminal to manually compile
   some classes that speclj needs:

    ```
    mkdir classes
    clojure -Rtest \
            -e "(dorun (map compile '(speclj.platform.SpecFailure
                                      speclj.platform.SpecPending)))"
    ```

5. Edit the `idiot_spec.clj` file, line 27, to bind `run` to `run-fast` instead
   of `run-safely`. Note that if you call `System/exit` in your code, the test
   run will be interrupted.

6. Normally, you could run `clojure -A:test` to see if everything worked, but
   this test suite will actually wipe out your `.git` directory. (Sorry.) If
   you want to avoid that, run this command in a bash-compatible shell instead:

    ```
    rm -rf test-run && \
      mkdir test-run && \
      cp -r src spec deps.edn test-run/ && \
      (cd test-run ; \
       mkdir classes; \
       clj -Rtest -e "(dorun (map compile
                                  '(speclj.platform.SpecFailure
                                    speclj.platform.SpecPending)))" && \
       clj -A:test)
    ```

    (You could also save that as a separate script, like `run-tests.sh`, to cut
    down on the noise, then call it with `bash run-tests.sh`.)

    If you got any output from speclj (even if it reports test
    failures), you're setup correctly. If not, pay attention to what error
    message is reported. If you can't figure it out, come to office hours or
    post something on Piazza.

7. For a better development workflow, pass the `-c` and `-a` switches. The `-c`
   switch colorizes the output. The `-a` switch starts an auto-runner, which
   reruns the tests whenever your source code changes.

You can also (safely) run the style checks locally like so (in a
bash-compatible shell):

    find src -name \*.clj | \
      xargs clojure -Sdeps '{:deps {cljfmt {:mvn/version "RELEASE"}}}' \
                    -m cljfmt.main check

and

    clojure -Sdeps '{:deps {clj-kondo {:mvn/version "RELEASE"}}}' \
            -m clj-kondo.main --lint src

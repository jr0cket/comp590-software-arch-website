---
title: Assignment 1: Clojure Practice
stem: clojure-practice
---

_Update Monday, Feb. 17: the [solution](../assn1-solution.zip) is now
available._

### Due

The last second of Monday, February 3<sup>rd</sup>.

### Context

You'll be writing a fair bit of Clojure this semester. This assignment gives
you some practice before we get into the more substantial assignments related
to git.

### Requirements

First, you'll need to get Clojure running on your machine. Ask LAs or Jeff for
help if you have trouble.

Write a program in Clojure to be invoked by the `clojure` command-line runner.
There should be a directory named `src`, which should contain a file named
`mishmash.clj`. That file should define the Clojure namespace `mishmash`. The
namespace should have a `-main` function that accepts a variable number of
arguments (see hints below), which are provided by the `clojure` command-line
runner from the command, as strings. You may use other namespaces if you like,
but you don't have to. If you've set this up correctly, you should be able to
invoke your `-main` function with the command `clojure -m mishmash` run from
the project root directory (i.e. the directory containing `src/`).

The first argument to the script is the sub-command to invoke, as described
below. Further arguments are for the sub-command.

All output should be on a single line terminated by a newline. Nothing should
be printed apart from what's required.

#### 1. Pascal's Triangle

Write a sub-program, accessible with the `pascal` sub-command, that accepts an
integer argument &ge; 0 and prints space-separated values of the corresponding
row of [Pascal's triangle](https://en.wikipedia.org/wiki/Pascal%27s_triangle).
For example, the command `clojure -m mishmash pascal 2` should print `1 2 1`.
If invalid input is given to the command, print `invalid input` instead.

#### 2. Write Roman Numerals

Write a sub-program, accessible with the `write-roman` sub-command, that
accepts an integer argument &ge; 1 and &lt; 4,000 and prints the corresponding
[Roman numeral](https://en.m.wikipedia.org/wiki/Roman_numerals) in traditional
&ldquo;subtractive&rdquo; notation, where e.g. 4 is represented as `IV` rather
than `IIII`. For example, the command `clojure -m mishmash write-roman 140`
should print `CXL`. If invalid input is given to the command, print `invalid
input` instead.

#### 3. Read Roman Numerals

Write a sub-program, accessible with the `read-roman` sub-command, that accepts
a Roman numeral and prints the corresponding integer in traditional decimal
notation. For example, the command `clojure -m mishmash read-roman XLVIII`
should print `48`. If invalid input is given to the command, print `invalid
input` instead.

### Submission instructions

You should have gotten an email from Gradecope with an invitation to the
course; let me know if not. From there, hopefully it's fairly obvious how to
submit your code. Please post to Piazza if not.

You will need to submit your code as a Clojure deps project. What this means is
that the top-level directory of the repository or .zip file that you submit to
Gradescope should have a `src` directory with at least a `mishmash.clj` file in
it as well as a `deps.edn` file whose contents should be at least `{}`. If your
submission is missing these elements, or if they're not in the top-level (root)
directory, then the autograder will not work.

### Hints

- From what I can tell, many people like the Cursive IDE for Clojure
  development, which is part of the JetBrains IDE suite that some of you may be
  familiar with from Java development. It is free for academic use.
- To handle a variable number of arguments in a Clojure function, which is
  useful e.g. for the `mishmash/-main` function, use the `&` &ldquo;rest
  operator&rdquo; to wrap however many arguments there happen to be into a
  sequence; in other words, your program should, presumably at the bottom,
  contain `(defn -main [& args] ,,,)`, where `,,,` is a Clojure-flavored
  ellipsis (remember that commas in Clojure are whitespace).
- For Pascal's triangle, you may prefer to use recursion, or you may prefer to
  use [the binomial
  coefficient](https://en.wikipedia.org/wiki/Binomial_coefficient) formula.
  Either is permissible.
- Remember that Clojure filenames *mostly* correspond to Clojure namespaces,
  except that dashes (`-`) in namespaces must be converted to underscores (`_`)
  in filenames. (This is a limitation imposed by the JVM.)
- You may prefer to write your own tests, for faster feedback than what
  Gradescope provides. If so, [the Cognitect test
  runner](https://github.com/cognitect-labs/test-runner) library and [the
  clojure.test
  namespace](http://clojure.github.io/clojure/clojure.test-api.html) (which is
  built-in to Clojure so doesn't require external dependencies) is a
  lightweight way to get some tests up and running.
- Remember that your standard output is captured by the test runner and
  compared against the expected value. If you want to print something that
  isn't captured, e.g. for debugging purposes, print to the standard error
  stream instead by wrapping your calls to printing statements in a `(binding
  [*out* *err*] ,,,)` form, e.g. `(binding [*out* *err*] (prn 'args args))` to
  print the value of the `args` var.

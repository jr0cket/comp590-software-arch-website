---
title: Notes from Feb 17 lecture
stem: notes-feb-17
---

Pre-class joke: [Rise of the
Machines](https://www.smbc-comics.com/comic/rise-of-the-machines), from
_Saturday Morning Breakfast Cereal_ by Zach Weinersmith.

### Announcements

- A2 due tonight at 11:59:59pm!
- Exam 2 is next Wednesday

### Assignment 1 review

See the [Assignment 1 solution zip](../assn1-solution.zip).

### Learning objectives for last class

- given some code, list behaviors as pending tests
- given a pending test, implement the test
- given a test, write code that passes the test

### Review exercise from last lecture

Given this code:

```clojure
(ns test-demo)

(defn summation
  ([xs] (summation identity xs))
  ([f xs] (reduce + 0 (map f xs))))
```

And these pending tests:

```clojure
(ns test-demo-spec
  (:require [speclj.core :refer [describe it should should=]]
            [test-demo :as sut]))

(describe "summation"
  (it "returns 0 given an empty sequence")
  (it "maps the given fn arg over the sequence before summing")
  (it "sums the items of the mapped sequence"))
```

Implement the tests to get to green.

Here's my implementation:

```clojure
(describe "summation"
  (it "returns 0 given an empty sequence"
    (should= 0 (sut/summation [])))

  (it "maps the given fn arg over the sequence before summing"
    (should= 5 (sut/summation inc [4])))

  (it "sums the items of the mapped sequence"
    (should= 9 (sut/summation inc [3 4]))))
```

### Learning objectives for this class

- define "test double"
- write code to use test doubles with Clojure and speclj
- compare and contrast isolated vs. non-isolated tests

### Isolated test tutorial with sum/mean/stdev

I started with this code in `src/test_demo.clj`:

```clojure
(ns test-demo)

(defn sum [xs]
  (loop [accum 1
         items xs]
    (if (empty? items)
      accum
      (recur (+ accum (first items))
             (rest items)))))

(defn mean [xs]
  (when (not-empty xs)
    (/ (sum xs)
       (count xs))))

(defn stdev [xs]
  (when (not-empty xs)
    (if (empty? (rest xs))
      0.0
      (Math/sqrt
        (* (/ 1 (dec (count xs)))
           (sum (let [square #(* % %)]
                  (map square
                       (let [mu (mean xs)]
                         (map #(- % mu) xs))))))))))
```

...and this code in `spec/test_demo_spec.clj`:

```clojure
(ns test-demo-spec
  (:require
    [test-demo :as sut]
    [speclj.core
     :refer [describe
             it
             should-have-invoked
             should-not-have-invoked
             should=
             stub
             with-stubs]]))

(describe "sum"
  (it "returns 0 given an empty sequence"
    (should= 0 (sut/sum [])))
  (it "returns the only item of a 1-item sequence"
    (should= 7 (sut/sum [7])))
  (it "adds both items in a 2-item sequence"
    (should= 6 (sut/sum [4 2])))
  (it "sums a bunch of numbers"
    (should= 5050 (sut/sum (range 1 101)))))

(describe "mean"
  (it "returns nil given an empty sequence"
    (should= nil (sut/mean [])) )
  (it "returns the only item of a 1-item sequence"
    (should= 17 (sut/mean [17])))
  (it "returns the correct mean of a big sequence"
    (should= 5050/100 (sut/mean (range 1 101)))))

(describe "stdev"
  (it "returns nil given an empty sequence"
    (should= nil (sut/stdev [])))
  (it "returns 0 for a sequence of 1 item"
    (should= 0.0 (sut/stdev [7])))
  (it "returns the correct stdev for a small sequence"
    (should= 1.0 (sut/stdev [3 4 5])))
  (it "returns the correct stdev for a big sequence"
    (should= 29.01149197588202 (sut/stdev (range 1 101)))))
```

This is essentially what we had last time, with the addition of code and tests
for a standard deviation function.

The difference is that there's a bug in the code somewhere. Here's the output
of the tests:

```
sum
- returns 0 given an empty sequence (FAILED)
- returns the only item of a 1-item sequence (FAILED)
- adds both items in a 2-item sequence (FAILED)
- sums a bunch of numbers (FAILED)

mean
- returns nil given an empty sequence
- returns the only item of a 1-item sequence (FAILED)
- returns the correct mean of a big sequence (FAILED)

stdev
- returns nil given an empty sequence
- returns 0 for a sequence of 1 item
- returns the correct stdev for a small sequence (FAILED)
- returns the correct stdev for a big sequence (FAILED)

Failures:

  1) sum returns 0 given an empty sequence
     Expected: 0
          got: 1 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:16

  2) sum returns the only item of a 1-item sequence
     Expected: 7
          got: 8 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:18

  3) sum adds both items in a 2-item sequence
     Expected: 6
          got: 7 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:20

  4) sum sums a bunch of numbers
     Expected: 5050
          got: 5051 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:22

  5) mean returns the only item of a 1-item sequence
     Expected: 17
          got: 18 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:111

  6) mean returns the correct mean of a big sequence
     Expected: 101/2
          got: 5051/100 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:113

  7) stdev returns the correct stdev for a small sequence
     Expected: 1.0
          got: 1.2909944487358058 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:122

  8) stdev returns the correct stdev for a big sequence
     Expected: 29.01149197588202
          got: 29.011667802573307 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:124

Finished in 0.00391 seconds
11 examples, 8 failures
```

Now, we could think about what problem this code is solving, and how it might
be solving it. We might realize, either just by thinking about it, or by
looking at the implementation, that `stdev` calls `mean` and `sum`, and `mean`
calls `sum`, and `sum` doesn't call anything. Since `sum` is the common factor
in all of these failures, we might conclude that `sum` is likely where the bug
is. This is not wrong.

However, let's throw that analysis out the window. Why? Because in a larger, or
more complex, or less familiar code base, we wouldn't easily be able to do an
analysis like that. And we'd be stuck with a wall of failures, trying to get
our heads around not only the failures but also the behaviors of the various
functions at play so that we could figure out a likely place to start looking.

This is not a fun place to be. I mean, at least we _have_ tests to tell us that
there's a problem and give us some information to work with. But 8 failures,
including many that (it turns out) only _use_ the function with the bug rather
than themselves containing a bug--it's not an ideal scenario. And, at least
from the perspective of the number of failures, we can do better.

#### Introducing isolated tests and test doubles

To do so, we use a different testing technique: _isolated tests_. In isolated
tests, each test only depends on the function being testing. Nothing else.
Let's see how to do this in Clojure and Speclj.

First of all, note that `sum` is, vacuously, an isolated test, because it
doesn't depend on anything else. I mean, technically, it depends on various
core functions. But we don't need to test those. (Remember that we test _our_
code, not the code of others. And core functions in particular are very
unlikely to have a bug; they're battle-tested.)

Let's make the tests for `mean` isolated. To do that, we're going to use a
_test double_. The idea here is like a stunt double in a movie. The character
needs to do something dangerous, and the stunt double stands in for the actor
to do the stunt, in a way that can pass for the actor, so that the actor
doesn't get exposed to the danger. Test doubles are like that: stand-ins for
the real thing.

There are various kinds of test doubles. Professionals have various shades of
meaning and specialized words here, like "stub" and "mock" and "dummy". We're
not going to cover all of that here. But they're all within the broader
category of "test doubles".

#### Isolated tests and stubs with Speclj

In this case, to isolate `mean`, we need a test double for `sum`, so that the
tests can run without actually calling `sum`. Here's how to do that with
Speclj:

```clojure
(describe "mean (isolated test)"
  (with-stubs)

  (it "calls sum with the same arguments"
    (let [seq (range 10)]
      (with-redefs [sut/sum (stub :sum
                                  {:return 0})]
        (sut/mean seq))
      (should-have-invoked :sum {:with [seq]}))))
```

Let's break this down. The core of this is a Clojure macro called `with-redefs`
that redefines the given var(s) during the body. The body calls `sut/mean`.
During that call, the `sut/sum` var is redefined, so that the `sum` function we
defined in the `test-demo` namespace isn't actually called. Instead, we use
Speclj to call a stub. A stub is a function that by default does nothing except
track its invocations and arguments and return `nil`. In this case, we name the
stub `:sum` (a name that we'll use later) and tell the stub to return `0`
instead of `nil` so that `mean` doesn't throw an exception when it tries to
divide `nil` by a number.

Once we've called `sut/mean`, we can let `sut/sum` revert back to its original
value. Then we make our assertion that the `:sum` stub was invoked with a
single argument of `seq`. (Another slightly more accurate way to say this is
that the stub was invoked with an arglist of `[seq]`.) If it was, our behavior
is satisfied, and the test passes.

Notice that we have to call `(with-stubs)` in the body of the enclosing
`describe` block. This is necessary so that Speclj will set up the necessary
machinery to use stubs. Without it, you get a rather confusing error message
that an unbound var cannot be cast to a future.

Let's define another test case.

```clojure
  (it "does not call sum when the sequence is empty"
    (with-redefs [sut/sum (stub :sum
                                {:return 0})]
      (sut/mean []))
    (should-not-have-invoked :sum))
```

We have a similar `with-redefs` block with a body that calls `sut/mean`. This
time, though, we say that the `:sum` stub should not have been invoked.

A couple more test cases should round out the behaviors for isolated tests of
`mean`:

```clojure
(it "returns nil given an empty sequence"
  (with-redefs [sut/sum (stub :sum
                              {:return 0})]
    (should= nil (sut/mean []))))

(it "returns the correct mean of a larger sequence"
  (with-redefs [sut/sum (stub :sum
                              {:return 5050})]
    (should= 5050/100 (sut/mean (range 1 101)))))
```

In these cases, note that we are again testing the returned value of `mean`.
The difference is that these tests are isolated and careful to use
`with-redefs` to avoid actually calling the real `sut/sum` function.

Now what is the test output?

```
sum
- returns 0 given an empty sequence (FAILED)
- returns the only item of a 1-item sequence (FAILED)
- adds both items in a 2-item sequence (FAILED)
- sums a bunch of numbers (FAILED)

mean (isolated test)
- does not call sum when the sequence is empty
- calls sum with the same arguments
- returns nil given an empty sequence
- returns the correct mean of a larger sequence

stdev
- returns nil given an empty sequence
- returns 0 for a sequence of 1 item
- returns the correct stdev for a small sequence (FAILED)
- returns the correct stdev for a big sequence (FAILED)

Failures:

  1) sum returns 0 given an empty sequence
     Expected: 0
          got: 1 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:16

  2) sum returns the only item of a 1-item sequence
     Expected: 7
          got: 8 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:18

  3) sum adds both items in a 2-item sequence
     Expected: 6
          got: 7 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:20

  4) sum sums a bunch of numbers
     Expected: 5050
          got: 5051 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:22

  5) stdev returns the correct stdev for a small sequence
     Expected: 1.0
          got: 1.2909944487358058 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:122

  6) stdev returns the correct stdev for a big sequence
     Expected: 29.01149197588202
          got: 29.011667802573307 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:124

Finished in 0.00280 seconds
12 examples, 6 failures
```

We still have some test failures for `stdev`, which isn't isolated yet. And of
course we have some failures for `sum`, which actually contains the bug. But we
have no failures for `mean`. The `mean` function doesn't actually contain a
bug, so this test output more accurately reflects reality.

Without spending much time explaining this, here are some isolated tests for
`stdev`:

```clojure
(describe "stdev (isolated)"
  (with-stubs)

  (it "does not call sum or mean when seq is empty"
    (let [sum-stub (stub :sum)
          mean-stub (stub :mean)]
      (with-redefs [sut/sum sum-stub
                    sut/mean mean-stub]
        (sut/stdev [])
        (should-not-have-invoked :sum)
        (should-not-have-invoked :mean))))

  (it "does not call sum or mean when seq has 1 item"
    (let [sum-stub (stub :sum)
          mean-stub (stub :mean)]
      (with-redefs [sut/sum sum-stub
                    sut/mean mean-stub]
        (sut/stdev [7])
        (should-not-have-invoked :sum)
        (should-not-have-invoked :mean))))

  (it "returns nil given an empty sequence"
    (let [sum-stub (stub :sum)
          mean-stub (stub :mean)]
      (with-redefs [sut/sum sum-stub
                    sut/mean mean-stub]
        (should= nil (sut/stdev [])))))

  (it "returns 0 for a sequence of 1 item"
    (let [sum-stub (stub :sum)
          mean-stub (stub :mean)]
      (with-redefs [sut/sum sum-stub
                    sut/mean mean-stub]
        (should= 0.0 (sut/stdev [7])))))

  (it "sums the squares of the mean-centered seq"
    (let [xs [7 8]
          sum-stub (stub :sum {:return (apply + xs)})
          mean-stub (stub :mean {:return -1})]
      (with-redefs [sut/sum sum-stub
                    sut/mean mean-stub]
        (sut/stdev xs)
        (should-have-invoked :sum {:with [(->> xs
                                               (map inc)
                                               (map #(* % %)))]}))))

  (it "returns the correct stdev for a small sequence"
    (let [xs [3 4 5]
          sum-stub (stub :sum {:return 2})
          mean-stub (stub :mean {:return 4})]
      (with-redefs [sut/sum sum-stub
                    sut/mean mean-stub]
        (should= 1.0 (sut/stdev xs))))))
```

And now our test output looks like this:

```
sum
- returns 0 given an empty sequence (FAILED)
- returns the only item of a 1-item sequence (FAILED)
- adds both items in a 2-item sequence (FAILED)
- sums a bunch of numbers (FAILED)

mean (isolated test)
- does not call sum when the sequence is empty
- calls sum with the same arguments
- returns nil given an empty sequence
- returns the correct mean of a larger sequence

stdev (isolated)
- does not call sum or mean when seq is empty
- does not call sum or mean when seq has 1 item
- returns nil given an empty sequence
- returns 0 for a sequence of 1 item
- sums the squares of the mean-centered seq
- returns the correct stdev for a small sequence

Failures:

  1) sum returns 0 given an empty sequence
     Expected: 0
          got: 1 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:16

  2) sum returns the only item of a 1-item sequence
     Expected: 7
          got: 8 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:18

  3) sum adds both items in a 2-item sequence
     Expected: 6
          got: 7 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:20

  4) sum sums a bunch of numbers
     Expected: 5050
          got: 5051 (using =)
     /Users/jeff/code/590/cursive/test-demo/spec/test_demo_spec.clj:22

Finished in 0.00555 seconds
14 examples, 4 failures
```

Now, with fully isolated tests, our test output is a smoking gun that points to
the bug being in the `sum` function. Sure enough, a closer examination of the `sum` function reveals the bug:

```clojure
(defn sum [xs]
  (loop [accum 1
         items xs]
    (if (empty? items)
      accum
      (recur (+ accum (first items))
             (rest items)))))
```

The bug is that the accumulator starts at 1 instead of 0. Fixing that, all our
tests pass.

#### Behaviors in isolated tests

Before we move on, let's reflect. One difference with isolated tests is that
the behaviors are different. Now, rather than merely caring what a function
returns given certain arguments, we care what other functions a function calls.
Before, it was nice to only consider the interface of the function (i.e. what
it accepts and returns) when writing tests. That's the same thing that callers
of the function have to consider. But with isolated tests, we have to care how
the function is implemented. And we're tied to the particular implementation,
so that if the implementation details change, our tests might break
unnecessarily. We'll see more about the tradeoffs of isolated tests below. But
for now, suffice it to say that isolated tests care about returned values _and
also_ what other functions are called by a function, and how they're called.

### Exercise: list behaviors of `pascal-row` assuming isolated tests

### Real-world testing examples

- Massive breakage from 1 small bug in [SSG
  repo](https://gitlab.com/unc-app-lab/clj-ssg) (I changed the
  `edu.unc.applab.ssg.util/slugify` function to use underscores instead of
  dashes.)
- Isolated tests in [Clem test
  suite](https://gitlab.com/unc-app-lab/clem/-/blob/35f6dee26fb47f8a8044e524a863ce3271838a66/spec/edu/unc/applab/clem/db/exception_finding_spec.clj#L207)

### Test isolation tradeoffs

Benefits of isolated tests:

- You are really only testing what's in the function.
    - Example: with non-isolated tests for `mean`, we were implicitly testing
      the `sum` function again.
    - For complex functions that call complex functions, it can be easier to
      have an isolated test.
- When a bug happens, only relevant tests fail.
- Isolated tests can skip side effects, e.g.
    - sending an email
    - writing a record to a database
- Isolated tests can therefore be faster.

Costs of isolated tests:

- It's more complicated to set up test doubles.
- You can't treat your function as a black box. You're depending not just on
  the interface but also on the implementation of your function. So you have to
  know more to write isolated tests, and your test embodies more knowledge
  about what's being tested.

Recommendations:

- If:
    - it's easier to think about the calling patterns of a function than what
      the right answer is, or
    - you have side effects that you want to avoid during testing, or
    - your tests are too slow:
- consider isolated tests.
- Otherwise, use non-isolated tests.

### Exercise: isolated test implementation

> Which of the following is a valid isolated test for the `pascal-row` function
> in this code:

```clojure
(defn pascal-row
  "Return a seq of the numbers in the n-th row of Pascal's triangle (where n=0 is
  the first row)."
  [n]
  (for [i (range (inc n))]
    (combinations n i)))
```

A.

```clojure
(it "returns the base case row"
  (should= [1] (sut/pascal-row 0)))
```

B.

```clojure
(it "calls `combinations` n+1 times"
  (with-redefs [sut/combinations (stub :combinations {:returns 1})]
    (sut/pascal-row 4))
  (should-have-invoked :combinations {:times 5}))
```

C.

```clojure
(it "returns a non-trivial row"
  (should= [1 4 6 4 1] (sut/pascal-row 4)))
```

D.

```clojure
(it "calls `combinations` with different arguments each time"
  (with-redefs [sut/combinations (stub :combinations {:returns 1})]
    (sut/pascal-row 1))
  (should-have-invoked :combinations {:with [1 0]})
  (should-have-invoked :combinations {:with [1 1]}))
```

Answer(s): B and D are correct. All of these are valid, but B and D are
isolated tests like the question asked for.

### Take-away questions


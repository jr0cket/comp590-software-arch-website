---
title: Notes from Jan. 15 lecture
stem: notes-jan-15
---

### Announcements

- Music: Prelude in C Major by J.S. Bach
    - [https://www.youtube.com/watch?v=frxT2qB1POQ](https://www.youtube.com/watch?v=frxT2qB1POQ)
    - It's simple, elegant, and beautiful&emdash;like Clojure.
- Making progress on the autograder, but not there yet.
- LA office hours (in SN141) posted on website (and subject to change).
- Go easy on LAs; they're learning Clojure too :-)
- Who's interested in cohorts? (Poll to collect interest.)
- Poll to see where y'all are in your Clojure installations.
- Note: when pairing on a pollev question, both people should submit (for
  class participation credit).

### Warm-up exercise

> Pair up. Like before, write a function to square a number and apply it to the
> number 16, but this time, use a let block to bind your function to the name
> `square` before calling it.

```clojure
(let [square (fn [x] (* x x))]
  (square 16))
```

### 8. conditionals, redux

```clojure
nil, false          ; the only falsy values in Clojure
(and nil (/ 1 0))   ; nil (short-circuits)
(or true (/ 1 0))   ; true (short-circuits)
(not 7)             ; false
(and [] 7)          ; 7 (if all true, returns last arg)
(or nil [])         ; [] (if any true, returns truthy arg)
(and 1 2 3 4)       ; "variadic": accepts any "arity" or # of args
(or nil nil nil 5)  ; ditto
cond                ; for if/elseif/elseif/else conditions
(cond
  (>= grade 90) :A  ; short-circuits: only first true branch fires
  (>= grade 80) :B
  (>= grade 70) :C
  (>= grade 60) :D
  :else         :F) ; `:else` is arbitrary, but clear
(do :expr1 :expr2 ,,,) ; combine several exprs into 1; return last evaluation
(if (even? 7)
  (do
    (println "7 was even!")
    :even)
  (do
    (println "no surprises")
    :odd))
```

### git interlude 1

- git is "a stupid content tracker" (type `man git` in a terminal).
- Note: I'm talking about git, the CLI tool, not GitHub, the web service.
- Lowest foundation of git: a content-addressable database of objects.
- Simplest type of object: a blob: the contents of a file.
- What does content-addressable mean? Let's investigate.
- In a terminal window running a bash-compatible shell:

```sh
mkdir git-demo
cd git-demo
git init
tree .git  # to see what `git init` created
echo <(echo hello)  # to see how to create pseudo files
cat <(echo hello)  # fakes a file with contents of "hello\n"
git hash-object <(echo hello)  # compute the address of a blob
git hash-object -w <(echo hello)  # write the blob to the database
git cat-file -p <address>  # show the contents of the blob in the DB
```

Summary:

- git uses a content-addressable database of objects
- one type of object is "blob", which is a snapshot of file contents

### 9. sequences

```clojure
(def s (range 5))
s                   ; '(0 1 2 3 4)
(count s)           ; 5
(first s)           ; 0
(rest s)            ; '(1 2 3 4)
(nth s 3)           ; 3
(cons -1 s)         ; '(-1 0 1 2 3 4)
(conj s -1)         ; '(-1 0 1 2 3 4)
;; Note: conj adds to the start of a list, end of a vector
;; conj is efficient, not consistent
(empty? s)          ; false
(empty? ())         ; true
(map inc s)         ; '(1 2 3 4 5)
(for [x s] (inc x)) ; '(1 2 3 4 5) (a "list comprehension")
(map #(map inc (range %))
     (range 3))     ; '(() (1) (1 2))
(for [n (range 3)
      i (range n)]
  (inc i))          ; '(1 1 2) - notice: no nesting
(filter odd? s)     ; '(1 3)
(reduce + 0 s)      ; 10
;; let's trace it out:
(reduce +    0    (range 5))
(reduce +    0    '(0 1 2 3 4))
(reduce + (+ 0 0) '(1 2 3 4))
(reduce + (+ 0 1) '(2 3 4))
(reduce + (+ 1 2) '(3 4))
(reduce + (+ 3 3) '(4))
(reduce + (+ 6 4) '())
(reduce +   10    '())
10
```

### 10. hashmaps

```clojure
(def hm {:foo 1, :bar 2})
(keys hm)            ; '(:foo :bar)
(vals hm)            ; '(1 2)
(first hm)           ; '(:foo 1)
(key (first hm))     ; :foo
(val (first hm))     ; 1
(count hm)           ; 2
(get hm :foo)        ; 1
(assoc hm :baz 3)    ; _assoc_iate new entry
hm                   ; hashmaps are immutable
(dissoc hm :bar)     ; {:foo 1}
(update hm :bar inc) ; {:foo 1, :bar 3}
(def people {:jeff {:name "Jeff", :origin :tx}
             :henry {:name "Henry", :origin :nc}})
(get-in people [:jeff :origin])  ; :tx
(assoc-in people [:jeff :origin] :ar)
```
